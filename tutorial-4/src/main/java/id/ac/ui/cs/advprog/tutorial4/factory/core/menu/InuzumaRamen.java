package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public class InuzumaRamen extends MenuFactory {
    //Ingridients:
    //Noodle: Ramen
    //Meat: Pork
    //Topping: Boiled Egg
    //Flavor: Spicy
    public InuzumaRamen(String name){
        // super(name);
    }

    public Noodle createNoodle() {
        return new Ramen();
    }

    public Meat createMeat() {
        return new Pork();
    }

    public Topping createTopping() {
        return new BoiledEgg();
    }

    public Flavor createFlavor() {
        return new Spicy();
    }
}
package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class OrderDrink {

    private String drink = null;

    private static OrderDrink orderDrink;

    private OrderDrink(){
        try{
            System.out.println("Creating Order Drink.....");
            Thread.sleep(2000);
            System.out.println("Done Order Drink.....");
        } catch(InterruptedException e){
            e.printStackTrace();
        }
    }

    public static OrderDrink getInstance() {
        if (orderDrink == null){ 
            synchronized (OrderDrink.class){
                orderDrink = new OrderDrink();
                orderDrink.drink = "No Order yet. Order first.";
            }
        }
        return orderDrink;
    }

    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    @Override
    public String toString() {
        return drink;
    }

    public static void load(){};
}

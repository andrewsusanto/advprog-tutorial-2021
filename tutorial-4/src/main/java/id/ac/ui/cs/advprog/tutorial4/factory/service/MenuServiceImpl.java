package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MenuFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {
    private MenuRepository repo;

    public MenuServiceImpl(MenuRepository repo){
        this.repo = repo;
        initRepo();
    }

    public MenuServiceImpl(){
        this(new MenuRepository());
    }

    public Menu createMenu(String name, String type){

        MenuFactory factory = null;
        if(type.equals("LiyuanSoba")){
            factory = new LiyuanSoba(name);
        } else if(type.equals("InuzumaRamen")){
            factory = new InuzumaRamen(name);
        } else if(type.equals("MondoUdon")) {
            factory = new MondoUdon(name);
        } else {
            factory = new SnevnezhaShirataki(name);
        }

        Menu newMenu = createMenuFromFactory(factory, name);
        return repo.add(newMenu);
    }

    private Menu createMenuFromFactory(MenuFactory factory, String name){
        Noodle noodle = factory.createNoodle();
        Meat meat = factory.createMeat();
        Topping topping = factory.createTopping();
        Flavor flavor = factory.createFlavor();
        Menu newMenu = new Menu(name, noodle, meat, topping, flavor);
        return newMenu;
    }

    public List<Menu> getMenus(){
        return repo.getMenus();
    }

    private void initRepo(){
        createMenu("WanPlus Beef Mushroom Soba", "LiyuanSoba");
        createMenu("Bakufu Spicy Pork Ramen", "InuzumaRamen");
        createMenu("Good Hunter Cheese Chicken Udon", "MondoUdon");
        createMenu("Morepeko Flower Fish Shirataki", "SnevnezhaShirataki");
    }
}
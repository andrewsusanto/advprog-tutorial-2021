package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {
    public OrderServiceImpl() {
        OrderDrink.load();
        OrderFood.load();
    }

    public void orderADrink(String drink) {
        OrderDrink orderDrink = OrderDrink.getInstance();
        orderDrink.setDrink(drink);
    }
    
    public OrderDrink getDrink() {
        OrderDrink orderDrink = OrderDrink.getInstance();
        return orderDrink;
    }

    public void orderAFood(String food) {
        OrderFood orderFood = OrderFood.getInstance();
        orderFood.setFood(food);
    }
    
    public OrderFood getFood() {
        OrderFood orderFood = OrderFood.getInstance();
        return orderFood;
    }
}

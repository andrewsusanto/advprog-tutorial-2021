package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class OrderFood {

    private String food = "No Order yet. Order first.";

    private static OrderFood orderFood = new OrderFood();

    private OrderFood() {
        try{
            System.out.println("Creating Order Food.....");
            Thread.sleep(2000);
            System.out.println("Done Order Food....");
        } catch(InterruptedException e){
            e.printStackTrace();
        }
    }

    public static OrderFood getInstance() {
        return orderFood;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    @Override
    public String toString() {
        return food;
    }

    public static void load(){};
}

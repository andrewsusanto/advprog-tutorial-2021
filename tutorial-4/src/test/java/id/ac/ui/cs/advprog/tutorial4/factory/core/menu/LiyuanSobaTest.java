package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class  LiyuanSobaTest{

    private Class<?> liyuanSobaClass;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");
    }

    @Test
    public void testLiyuanSobaConcreteClass() {
        assertFalse(Modifier.isAbstract(liyuanSobaClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIngridients() throws Exception {
        MenuFactory liyuanSoba = new LiyuanSoba("soba");
        assertTrue(liyuanSoba instanceof MenuFactory);
        assertTrue(liyuanSoba.createNoodle() instanceof Soba);
        assertTrue(liyuanSoba.createMeat() instanceof Beef);
        assertTrue(liyuanSoba.createTopping() instanceof Mushroom);
        assertTrue(liyuanSoba.createFlavor() instanceof Sweet);
    }
}

package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class SpicyTest {

    private Class<?> spicyClass;

    @BeforeEach
    public void setUp() throws Exception {
        spicyClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy");
    }

    @Test
    public void testSpicyIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(spicyClass.getModifiers()));
    }

    @Test
    public void testSpicyIsFlavor() {
        Collection<Type> interfaces = Arrays.asList(spicyClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

    @Test
    public void testGetDescriptionOverrideMethod() throws Exception {
        Method getDescription = spicyClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
            getDescription.getParameterCount());
    }

    @Test
    public void testGetDescriptionContent() throws Exception {
        Spicy spicy = new Spicy();
        String description = spicy.getDescription();
        assertEquals("Adding Liyuan Chili Powder...", description);
    }
}

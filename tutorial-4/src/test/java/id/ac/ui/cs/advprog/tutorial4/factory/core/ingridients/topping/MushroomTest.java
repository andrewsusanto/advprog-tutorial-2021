package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class MushroomTest {

    private Class<?> mushroomClass;

    @BeforeEach
    public void setUp() throws Exception {
        mushroomClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom");
    }

    @Test
    public void testMushroomIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(mushroomClass.getModifiers()));
    }

    @Test
    public void testMushroomIsTopping() {
        Collection<Type> interfaces = Arrays.asList(mushroomClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping")));
    }

    @Test
    public void testGetDescriptionOverrideMethod() throws Exception {
        Method getDescription = mushroomClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getDescription.getGenericReturnType().getTypeName());
        assertEquals(0,
            getDescription.getParameterCount());
    }

    @Test
    public void testGetDescriptionContent() throws Exception {
        Mushroom mushroom = new Mushroom();
        String description = mushroom.getDescription();
        assertEquals("Adding Shiitake Mushroom Topping...", description);
    }
}

package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public class MenuTest {

    private Class<?> menuClass;

    @BeforeEach
    public void setUp() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
    }

    @Test
    public void testMenuIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(menuClass.getModifiers()));
    }

    @Test
    public void testGetNameMethod() throws Exception {
        Method getName = menuClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String", getName.getGenericReturnType().getTypeName());
        assertEquals(0, getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }
    @Test
    public void testGetNoodleMethod() throws Exception {
        Method getNoodle = menuClass.getDeclaredMethod("getNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle", getNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, getNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(getNoodle.getModifiers()));
    }
    @Test
    public void testGetMeatMethod() throws Exception {
        Method getMeat = menuClass.getDeclaredMethod("getMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat", getMeat.getGenericReturnType().getTypeName());
        assertEquals(0, getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(getMeat.getModifiers()));
    }
    @Test
    public void testGetToppingMethod() throws Exception {
        Method getTopping = menuClass.getDeclaredMethod("getTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping", getTopping.getGenericReturnType().getTypeName());
        assertEquals(0, getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(getTopping.getModifiers()));
    }

    @Test
    public void testGetTopping() throws Exception {
        Noodle noodle = new Ramen();
        Meat meat = new Pork();
        Topping topping = new BoiledEgg();
        Flavor flavor = new Spicy();

        Menu newMenu = new Menu("Menu", noodle, meat, topping, flavor);
        assertEquals(topping, newMenu.getTopping());
    }
    @Test
    public void testGetNoodle() throws Exception {
        Noodle noodle = new Ramen();
        Meat meat = new Pork();
        Topping topping = new BoiledEgg();
        Flavor flavor = new Spicy();

        Menu newMenu = new Menu("Menu", noodle, meat, topping, flavor);
        assertEquals(noodle, newMenu.getNoodle());
    }
    @Test
    public void testGetMeat() throws Exception {
        Noodle noodle = new Ramen();
        Meat meat = new Pork();
        Topping topping = new BoiledEgg();
        Flavor flavor = new Spicy();

        Menu newMenu = new Menu("Menu", noodle, meat, topping, flavor);
        assertEquals(meat, newMenu.getMeat());
    }
    @Test
    public void testGetFlavor() throws Exception {
        Noodle noodle = new Ramen();
        Meat meat = new Pork();
        Topping topping = new BoiledEgg();
        Flavor flavor = new Spicy();

        Menu newMenu = new Menu("Menu", noodle, meat, topping, flavor);
        assertEquals(flavor, newMenu.getFlavor());
    }

    @Test
    public void testGetName() throws Exception {
        Noodle noodle = new Ramen();
        Meat meat = new Pork();
        Topping topping = new BoiledEgg();
        Flavor flavor = new Spicy();

        Menu newMenu = new Menu("Xing Menu", noodle, meat, topping, flavor);
        assertEquals("Xing Menu", newMenu.getName());
    }
}

package id.ac.ui.cs.advprog.tutorial4.factory.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;

import java.lang.reflect.Method;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MenuServiceTest {

    @Mock
    private MenuRepository menuRepository;

    @InjectMocks
    private MenuServiceImpl menuService;

    private Class<?> menuServiceClass;

    @BeforeEach
    public void setUp() throws Exception {
        menuServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl");
    }

    @Test
    public void testGetMenusMethod() throws Exception {
        Method getMenus = menuServiceClass.getDeclaredMethod("getMenus");
        assertEquals(0,
            getMenus.getParameterCount());
    }

    @Test
    public void whenGetMenusCalledRepositoryGetMenus() {
        menuService.getMenus();
        verify(menuRepository, times(1)).getMenus();
    }

    @Test
    public void testMenuServiceCreateMenuCallCreateMenu() {
        MenuServiceImpl mn = Mockito.mock(MenuServiceImpl.class);
        mn.createMenu("SpecialUdon", "MondoUdon");
        verify(mn, atLeast(1)).createMenu(Mockito.anyString(), Mockito.anyString());
    }

    @Test 
    public void testMenuServiceCreateMenuSize() {
        MenuServiceImpl mn = new MenuServiceImpl();
        mn.createMenu("SpecialUdon", "MondoUdon");
        List<Menu> menus = mn.getMenus();
        assertEquals(5, menus.size());
    }

}

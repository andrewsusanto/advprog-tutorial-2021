package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Modifier;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;

public class  MondoUdonTest{

    private Class<?> mondoUdonClass;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
    }

    @Test
    public void testMondoUdonConcreteClass() {
        assertFalse(Modifier.isAbstract(mondoUdonClass.getModifiers()));
    }

    @Test
    public void testMondoUdonIngridients() throws Exception {
        MenuFactory mondoUdon = new MondoUdon("udon");
        assertTrue(mondoUdon instanceof MenuFactory);
        assertTrue(mondoUdon.createNoodle() instanceof Udon);
        assertTrue(mondoUdon.createMeat() instanceof Chicken);
        assertTrue(mondoUdon.createTopping() instanceof Cheese);
        assertTrue(mondoUdon.createFlavor() instanceof Salty);
    }
}

package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class  InuzumaRamenTest{

    private Class<?> inuzumaRamenClass;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
    }

    @Test
    public void testInzumaRamenConcreteClass() {
        assertFalse(Modifier.isAbstract(inuzumaRamenClass.getModifiers()));
    }

    @Test
    public void testInzumaRamenIngridients() throws Exception {
        MenuFactory inzumaRamen = new InuzumaRamen("ramen");
        assertTrue(inzumaRamen instanceof MenuFactory);
        assertTrue(inzumaRamen.createNoodle() instanceof Ramen);
        assertTrue(inzumaRamen.createMeat() instanceof Pork);
        assertTrue(inzumaRamen.createTopping() instanceof BoiledEgg);
        assertTrue(inzumaRamen.createFlavor() instanceof Spicy);
    }
}

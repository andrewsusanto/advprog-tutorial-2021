package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Modifier;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;

public class  SnevnezhaShiratakiTest{

    private Class<?> snevnezhaShiratakiClass;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");
    }

    @Test
    public void testSnevnezhaShiratakiConcreteClass() {
        assertFalse(Modifier.isAbstract(snevnezhaShiratakiClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIngridients() throws Exception {
        MenuFactory snevnezhaShirataki = new SnevnezhaShirataki("shirataki");
        assertTrue(snevnezhaShirataki instanceof MenuFactory);
        assertTrue(snevnezhaShirataki.createNoodle() instanceof Shirataki);
        assertTrue(snevnezhaShirataki.createMeat() instanceof Fish);
        assertTrue(snevnezhaShirataki.createTopping() instanceof Flower);
        assertTrue(snevnezhaShirataki.createFlavor() instanceof Umami);
    }
}

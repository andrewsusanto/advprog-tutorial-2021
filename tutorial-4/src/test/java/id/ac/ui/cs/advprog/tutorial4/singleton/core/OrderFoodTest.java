package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class OrderFoodTest {
    private Class<?> orderFoodClass;

    @InjectMocks
    private OrderFood orderFood;

    @BeforeEach
    public void setup() throws Exception {
        orderFoodClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood");
    }

    @Test
    public void testSingletonObject() {
        OrderFood orderFood = OrderFood.getInstance();
        OrderFood orderFood2 = OrderFood.getInstance();
        assertEquals(orderFood, orderFood2);
    }
}

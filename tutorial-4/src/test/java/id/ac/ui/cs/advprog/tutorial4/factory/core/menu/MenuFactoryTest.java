package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MenuFactoryTest {

    private Class<?> menuFactoryClass;

    @BeforeEach
    public void setUp() throws Exception {
        menuFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MenuFactory");
    }

    @Test
    public void testMenuIsAbstractClass() {
        assertTrue(Modifier.
                isAbstract(menuFactoryClass.getModifiers()));
    }

    @Test
    public void testCreateNoodleMethod() throws Exception {
        Method createNoodle = menuFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createNoodle.getParameterCount());
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle", createNoodle.getGenericReturnType().getTypeName());
    }
    @Test
    public void testCreateMeatMethod() throws Exception {
        Method createMeat = menuFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createMeat.getParameterCount());
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat", createMeat.getGenericReturnType().getTypeName());
    }
    @Test
    public void testCreateToppingMethod() throws Exception {
        Method createTopping = menuFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createTopping.getParameterCount());
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping", createTopping.getGenericReturnType().getTypeName());
    }
    @Test
    public void testCreateFlavorMethod() throws Exception {
        Method createFlavor = menuFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, createFlavor.getParameterCount());
        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor", createFlavor.getGenericReturnType().getTypeName());
    }
}

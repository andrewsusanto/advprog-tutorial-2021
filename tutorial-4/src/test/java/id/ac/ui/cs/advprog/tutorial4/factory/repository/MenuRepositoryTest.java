package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.stereotype.Repository;
import org.springframework.test.util.ReflectionTestUtils;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;

@Repository
public class MenuRepositoryTest {
    private MenuRepository menuRepository;

    @Mock
    private ArrayList<Menu> menus;

    private Menu sampleMenu;

    @BeforeEach
    public void setUp() {
        // bowRepository = new BowRepositoryImpl();
        // bows = new HashMap<>();
        // sampleBow = new IonicBow("Kocheng");
        // bows.put(sampleBow.getName(), sampleBow);
        menuRepository = new MenuRepository();
        Noodle noodle = new Ramen();
        Meat meat = new Pork();
        Topping topping = new BoiledEgg();
        Flavor flavor = new Spicy();
        sampleMenu = new Menu("Menu", noodle, meat, topping, flavor);
        menus = new ArrayList<Menu>();
        menus.add(sampleMenu);
        menuRepository.add(sampleMenu);
    }

    @Test
    public void whenGetMenusReturnAllMenus() {
        ReflectionTestUtils.setField(menuRepository, "list", menus);
        List<Menu> actualMenus = menuRepository.getMenus();
        
        assertEquals(menus, actualMenus);
    }
}

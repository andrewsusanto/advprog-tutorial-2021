package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {
    private Class<?> orderServiceClass;

    @InjectMocks
    private OrderServiceImpl orderService;

    @BeforeEach
    public void setup() throws Exception {
        orderServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
    }

    @Test
    public void testOrderServiceHasOrderADrinkMethod() throws Exception {
        Method orderADrink = orderServiceClass.getDeclaredMethod("orderADrink", String.class);
        int methodModifiers = orderADrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderServiceHasGetDrinkMethod() throws Exception {
        Method getDrink = orderServiceClass.getDeclaredMethod("getDrink");
        int methodModifiers = getDrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderServiceHasOrderAFoodMethod() throws Exception {
        Method orderAFood = orderServiceClass.getDeclaredMethod("orderAFood", String.class);
        int methodModifiers = orderAFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderServiceHasGetFoodMethod() throws Exception {
        Method getFood = orderServiceClass.getDeclaredMethod("getFood");
        int methodModifiers = getFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderAndGetDrinkMethodImplementedCorrectly() throws Exception {
        OrderDrink orderDrink = OrderDrink.getInstance();
        String drink = "Mocha";
        orderService.orderADrink(drink);
        assertTrue(orderDrink.getDrink().equals(drink));
        assertTrue(orderService.getDrink().equals(orderDrink));
    }

    @Test
    public void testOrderAndGetFoodMethodImplementedCorrectly() {
        OrderFood orderFood = OrderFood.getInstance();
        String food = "Spaghetti";
        orderService.orderAFood(food);
        assertTrue(orderFood.getFood().equals(food));
        assertTrue(orderService.getFood().equals(orderFood));
    }
}

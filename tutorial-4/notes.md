Perbedaan lazy instantiation dan eager instantiation pada Singleton Pattern adalah pada lazy instantiation, pembuatan objek dilakukan ketika pertama kali dibutuhkan / dipanggil, sehingga mungkin saja aplikasi berjalan tanpa objek diinstansiasi. Sedangkan pada eager instantiation, pembuatan objek dilakukan pada tahap awal instansiasi aplikasi, sehingga baik dibutuhkan maupun tidak, objek akan diinstansiasi di awal.

Keuntugan Lazy Instantiation:
- Aplikasi dapat dijalankan dengan lebih cepat, karena tidak perlu instansiasi objek di awal aplikasi
- Mengoptimasi memory yang digunakan, karena objek hanya dibuat ketika dibutuhkan

Kerugian Lazy Instantiation:
- User dapat mengalami waktu proses yang lama ditengah penggunaaan aplikasi sehingga mungkin menyebabkan ketidaknyamanan ketika mengguankan aplikasi

Keuntungan Eager Instantiation:
- Ketika aplikasi sudah dimulai, maka user tidak akan mengalami proses yang lama karena seluruh objek sudah siap digunakan

Kerugian Eager Instantiation:
- Ketika aplikasi dimulai, waktu loading lebih lambat karena harus menginisialisasi seluruh objek
- Memory yang digunakan dapat lebih besar karena seluruh objek diinisialisasi baik dibutuhkan maupun tidak

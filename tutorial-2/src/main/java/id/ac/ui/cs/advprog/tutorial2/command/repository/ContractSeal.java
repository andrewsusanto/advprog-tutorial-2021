package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {
    private Spell latestSpell;
    private int undoCount;
    private Map<String, Spell> spells;

    public ContractSeal() {
        spells = new HashMap<>();
        undoCount = 0;
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        Spell spell = spells.get(spellName);
        spell.cast();
        this.latestSpell = spell;
        resetUndoCount();
    }

    public void undoSpell() {
        if (undoCount < 2) {
            this.latestSpell.undo();
            increaseUndoCount();
        }
    }

    private void resetUndoCount() {
        undoCount = 0;
    }

    private void increaseUndoCount() {
       undoCount++; 
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}

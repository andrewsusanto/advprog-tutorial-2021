package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
    }

    public void update() {
        if (guild.getQuestType().equals("D") || guild.getQuestType().equals("E")) {
            this.addQuest(guild.getQuest());
        }
    }
}

package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import java.util.ArrayList;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.HorizontalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation;

public class FacadeTranslator {
    private AlphaCodex alphaCodex;
    private RunicCodex runicCodex;
    private ArrayList<Transformation> transformationList;
    
    public FacadeTranslator() {
        this.alphaCodex = AlphaCodex.getInstance();
        this.runicCodex = RunicCodex.getInstance();

        transformationList = new ArrayList<Transformation>();
        transformationList.add(new HorizontalTransformation());
        transformationList.add(new CelestialTransformation());
        transformationList.add(new AbyssalTransformation());
    }

    public String encode(String text) {
        Spell spell = new Spell(text, alphaCodex);
        for (int i = 0 ; i < transformationList.size() ; i++){
            spell = transformationList.get(i).encode(spell);
        }
        spell = CodexTranslator.translate(spell, runicCodex);
        return spell.getText();
    }

    public String decode(String text){
        Spell spell = new Spell(text, runicCodex);
        spell = CodexTranslator.translate(spell, alphaCodex);
        for (int i = transformationList.size()-1 ; i >= 0 ; i--){
            spell = transformationList.get(i).decode(spell);
        }
        return spell.getText();
    }
}

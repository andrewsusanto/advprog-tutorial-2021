package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class BowAdapter implements Weapon {
    private boolean isSpontant;
    private Bow bow;

    public BowAdapter(Bow bow) {
        this.isSpontant = true;
        this.bow = bow;
    }

    @Override
    public String normalAttack() {
        boolean isAimShot = !this.isSpontant;
        return bow.shootArrow(isAimShot);
    }

    @Override
    public String chargedAttack() {
        this.isSpontant = !this.isSpontant;
        if (this.isSpontant){
            return "[Mode Changed] Changed to spontant mode";
        }
        return "[Mode Changed] Changed to aim shot mode";
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        return bow.getHolderName();
    }
}

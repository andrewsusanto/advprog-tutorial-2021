package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
/**
 * Kelas ini mengimplementasikan sistem kriptografi Caesar cipher
*/
public class HorizontalTransformation implements Transformation{
    private int shift;

    public HorizontalTransformation(int key){
        this.shift = key % 26;
    }

    public HorizontalTransformation(){
        this(10);
    }

    @Override
    public Spell encode(Spell spell){
        return process(spell, true);
    }

    @Override
    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int shiftKey = encode ? shift : shift*-1;
        String res = "";

        for (int i = 0 ; i < text.length() ; i++){
            int ascii = text.charAt(i);
            int newAscii = ascii;
            // Only convert ascii code between a-z and A-Z
            if (ascii >= 65 && ascii <= 90){
                newAscii = ascii + shiftKey;
                while (newAscii < 65){
                    newAscii += 26;
                }
                while (newAscii > 90){
                    newAscii -= 26;
                }
            } else if (ascii >= 97 && ascii <= 122){
                newAscii = ascii + shiftKey;
                while (newAscii < 97){
                    newAscii += 26;
                }
                while (newAscii > 122){
                    newAscii -= 26;
                }
            }
            char newChar = (char) newAscii;
            res += newChar;
        }
        
        return new Spell(new String(res), codex);
    }
}

package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class SeawardPride implements Weapon {


    private String holderName;

    public SeawardPride(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String normalAttack() {
        return "With power of sea";
    }

    @Override
    public String chargedAttack() {
        return "Tsunami running to enemy";
    }

    @Override
    public String getName() {
        return "Seaward Pride - Weapon";
    }

    @Override
    public String getHolderName() { return holderName; }
}

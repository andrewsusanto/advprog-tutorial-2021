package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellBookRepository;

    private boolean adapterInitialized;

    public WeaponServiceImpl() {
        this.adapterInitialized = false; 
    }

    @Override
    public List<Weapon> findAll() {
        if (!adapterInitialized){
            initializeAdapter();
            this.adapterInitialized = true;
        }
        return weaponRepository.findAll();
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        weaponRepository.save(weapon);
        
        String attackRes;
        String attackTypeString;
        if (attackType == 0){
            attackRes = weapon.normalAttack();
            attackTypeString = "normal attack";
        } else {
            attackRes = weapon.chargedAttack();
            attackTypeString = "charged attack";
        }
        logRepository.addLog(
            String.format("%s attacked with %s (%s): %s", weapon.getHolderName(), weapon.getName(), attackTypeString,  attackRes)
        );
    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }

    private void initializeAdapter() {
        for (Bow bow : bowRepository.findAll()){
            Weapon bowToWeapon = new BowAdapter(bow);
            weaponRepository.save(bowToWeapon);
        }

        for (Spellbook spellbook : spellBookRepository.findAll()){
            Weapon spellbookToWeapon = new SpellbookAdapter(spellbook);
            weaponRepository.save(spellbookToWeapon);
        }
    }
}

package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class FullMoonPike implements Weapon {

    private String holderName;

    public FullMoonPike(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String normalAttack() {
        return "Bullet running from the moon to enemy";
    }

    @Override
    public String chargedAttack() {
        return "Bullet shower from the moon";
    }

    @Override
    public String getName() {
        return "Full Moon Pike - Weapon";
    }

    @Override
    public String getHolderName() { return holderName; }
}

package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class StaffOfHoumo implements Weapon {

    private String holderName;

    public StaffOfHoumo(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String normalAttack() {
        return "Eneny unconsious";
    }

    @Override
    public String chargedAttack() {
        return "Knocked out using no time";
    }

    @Override
    public String getName() {
        return "Staff of Houmo - Weapon";
    }

    @Override
    public String getHolderName() { return holderName; }
}

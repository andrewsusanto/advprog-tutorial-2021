package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {
    private Spellbook spellbook;
    private boolean lastAttackIsCharged;

    public SpellbookAdapter(Spellbook spellbook) {
        this.lastAttackIsCharged = false;
        this.spellbook = spellbook;
    }

    @Override
    public String normalAttack() {
        this.lastAttackIsCharged = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (!lastAttackIsCharged){
            this.lastAttackIsCharged = true;
            return spellbook.largeSpell();
        }
        return "[ATTACK FAILED] Cannot attack two consecutive spell charged attack";
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}

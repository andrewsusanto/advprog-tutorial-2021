package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class FesteringGreed implements Weapon {

    private String holderName;

    public FesteringGreed(String holderName) {
        this.holderName = holderName;
    }
    @Override
    public String normalAttack() {
        return "Boom boom boom !";
    }

    @Override
    public String chargedAttack() {
        return "CHERNOBYL LIKE BOMB";
    }

    @Override
    public String getName() {
        return "Festering Greed - Weapon";
    }

    @Override
    public String getHolderName() { return holderName; }
}

package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HorizontalTransformationTest {
    private Class<?> horizontalClass;

    @BeforeEach
    public void setup() throws Exception {
        horizontalClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.HorizontalTransformation");
    }

    @Test
    public void testHotizontalHasEncodeMethod() throws Exception {
        Method translate = horizontalClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testHorizontalEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Ckpsbk kxn S goxd dy k lvkmucwsdr dy pybqo yeb cgybn";

        Spell result = new HorizontalTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testHorizontalEncodesCorrectlyWithCustomKey() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Muzclu uhx C qyhn ni u vfuwemgcnb ni zilay iol mqilx";

        Spell result = new HorizontalTransformation(20).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testHorizontalHasDecodeMethod() throws Exception {
        Method translate = horizontalClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testHorizontalDecodesCorrectly() throws Exception {
        String text = "Ckpsbk kxn S goxd dy k lvkmucwsdr dy pybqo yeb cgybn";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new HorizontalTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testHorizontalDecodesCorrectlyWithCustomKey() throws Exception {
        String text = "Muzclu uhx C qyhn ni u vfuwemgcnb ni zilay iol mqilx";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new HorizontalTransformation(20).decode(spell);
        assertEquals(expected, result.getText());
    }
}

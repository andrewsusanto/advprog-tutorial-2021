package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class FacadeTranslatorTest {
    private Class<?> facadeTranslatorClass;

    @BeforeEach
    public void setUp() throws Exception {
        facadeTranslatorClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.facade.core.misc.FacadeTranslator");
    }

    @Test
    public void testFacadeTranslatorIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(facadeTranslatorClass.getModifiers()));
    }


    @Test
    public void testFacadeTranslatorEncodeMethod() throws Exception {
        Class<?>[] encodeArgs = new Class[1];
        encodeArgs[0] = String.class;
        Method encodeMethod = facadeTranslatorClass.getDeclaredMethod("encode", encodeArgs);

        assertEquals("java.lang.String",
            encodeMethod.getGenericReturnType().getTypeName());
        assertEquals(1,
            encodeMethod.getParameterCount());
        assertTrue(Modifier.isPublic(encodeMethod.getModifiers()));
    }

    @Test
    public void testFacadeTranslatorDecodeMethod() throws Exception {
        Class<?>[] decodeArgs = new Class[1];
        decodeArgs[0] = String.class;
        Method decodeMethod = facadeTranslatorClass.getDeclaredMethod("decode", decodeArgs);

        assertEquals("java.lang.String",
            decodeMethod.getGenericReturnType().getTypeName());
        assertEquals(1,
            decodeMethod.getParameterCount());
        assertTrue(Modifier.isPublic(decodeMethod.getModifiers()));
    }

    @Test
    public void testFacadeTranslatorEncodeDecodeMethod() {
        String stringToBeEncoded = "Lorem ipsum dolor sit amet consectetur adipiscing elit Duis eu erat consectetur tempor lorem eget ullamcorper quam Duis nisl erat scelerisque a dignissim acsagittis quis lorem";
        FacadeTranslator facadeTranslator = new FacadeTranslator();
        String encodeResult = facadeTranslator.encode(stringToBeEncoded);
        String decodeResult = facadeTranslator.decode(encodeResult);
        assertEquals(stringToBeEncoded, decodeResult);
    }
}

package id.ac.ui.cs.advprog.tutorial3.facade.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class FacadeServiceTest {
    private Class<?> facadeServiceClass;

    @InjectMocks
    private FacadeServiceImpl facadeService;

    @BeforeEach
    public void setup() throws Exception {
        facadeServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.service.FacadeServiceImpl");
    }

    @Test
    public void testFacadeServiceHasEncodeMethod() throws Exception {
        Method encode = facadeServiceClass.getDeclaredMethod("encode", String.class);
        int methodModifiers = encode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeServiceEncodeCorrectlyImplemented() {
        String result = facadeService.encode("Safira and I went to a blacksmith to forge our sword.");
        assertEquals("A#Z{;>a!{nX{G_B=i?$^.Bqv^_DeNHs)&v_FC|wH}qN^K@&;@cC/v", result);
    }

    @Test
    public void testFacadeServiceHasDecodeMethod() throws Exception {
        Method decode = facadeServiceClass.getDeclaredMethod("decode", String.class);
        int methodModifiers = decode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeServiceDecodeCorrectlyImplemented() {
        String result = facadeService.decode("vA#Z{>a!{nX{G_B=i?$^.Bqv^_DeNHs)&v_FC|wH}qN^K@&;@cC/");
        assertEquals("Safira and I went to a blacksmith to forge our sword", result);
    }

    @Test
    public void testFacadeServiceHasSetRequestTypeMethod() throws Exception {
        Method setRequestType = facadeServiceClass.getDeclaredMethod("setRequestType", String.class);
        int methodModifiers = setRequestType.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeServiceHasSetRequestValueMethod() throws Exception {
        Method setRequestValue = facadeServiceClass.getDeclaredMethod("setRequestValue", String.class);
        int methodModifiers = setRequestValue.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeServiceHasIsRequestEncodeMethod() throws Exception {
        Method isRequestEncode = facadeServiceClass.getDeclaredMethod("isRequestEncode");
        int methodModifiers = isRequestEncode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeServiceHasGetRequestValueMethod() throws Exception {
        Method getRequestValue = facadeServiceClass.getDeclaredMethod("getRequestValue");
        int methodModifiers = getRequestValue.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFacadeServiceIsRequestEncodeReturn() {
        facadeService.setRequestType("encode");
        assertTrue(facadeService.isRequestEncode());

        facadeService.setRequestType("decode");
        assertFalse(facadeService.isRequestEncode());
    }

    @Test
    public void testFacadeServiceGetRequestValueReturn() {
        String reqValue = "Safira and I went to a blacksmith to forge our sword";
        facadeService.setRequestValue(reqValue);
        assertEquals(reqValue, facadeService.getRequestValue());
    }
}

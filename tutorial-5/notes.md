# Requirements :

## App Background :
- Menawarkan lowongan mata kuliah yang membutuhkan asisten kepada mahasiswa
- Mahasiswa dapat melamar pada lowongan yang tersedia

## Models :

### Mata Kuliah
- KodeMatkul: String (Primary Key)
- namaMatkul: String
- prodi: String


### Mahsiswa 
- NPM: String (Primary Key)
- Nama : String
- Email: String
- ipk: String
- noTelp: String


### Log
- idLog: integer(Primary Key, auto increment)
- start: Datetime
- end: Datetime
- Deskripsi: Text


### Laporan Pembayaran
Sebuah laporan yang berisikan semua pekerjaan asisten pada setiap bulan, dan summary jumlah uang yang didapat
- Data bulan : String
- Jam kerja (Jam) : Float
- Pembayaran : Integer
* Pembayaran = 350 Greil / jam


## Restriction :
- Satu mata kuliah dapat menerima banyak mahasiswa
- Mahasiswa hanya dapat menjadi asisten di satu mata kuliah
- Mahasiswa yang mendaftar langsung diterima
- Mata kuliah yang sudah ada asisten tidak dapat dihapus
- Mahasiswa yang sudah menjadi asisten di suatu mata kuliah tidak dapat dihapus
- Mahasiswa dapat memperbaharui, dan menghapus log yang sudah dibuat
- Log dapat berisi lebih singkat daripada 1 jam

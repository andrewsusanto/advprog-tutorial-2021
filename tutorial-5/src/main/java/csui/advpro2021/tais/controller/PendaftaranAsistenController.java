package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.PendaftaranAsisten;
import csui.advpro2021.tais.service.MahasiswaService;
import csui.advpro2021.tais.service.MataKuliahService;
import csui.advpro2021.tais.service.PendaftaranAsistenService;

import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pendaftaran-asisten")
public class PendaftaranAsistenController {
    @Autowired
    private PendaftaranAsistenService pendaftaranAsistenService;

    @Autowired
    private MahasiswaService mahasiswaService;

    @Autowired
    private MataKuliahService mataKuliahService;

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getListPendaftaranAsisten(Authentication authentication) {
        return ResponseEntity.ok(pendaftaranAsistenService.getListPendaftaranAsisten());
    }

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createPendaftaranAsisten(@RequestBody PendaftaranAsisten pendaftaranAsisten, Authentication authentication) {
        String npmMahasiswa = (String) authentication.getPrincipal();
        if (!pendaftaranAsisten.getMahasiswa().getNpm().equals(npmMahasiswa)){
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        if (mataKuliahService.getMataKuliah(pendaftaranAsisten.getMataKuliah().getKodeMatkul()) == null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        Mahasiswa mahasiswa = pendaftaranAsisten.getMahasiswa();
        if (pendaftaranAsistenService.getPendaftaranAsisten(mahasiswa) != null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(pendaftaranAsistenService.createPendaftaranAsisten(pendaftaranAsisten));
    }

    @GetMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getPendaftaranAsisten(@PathVariable(value = "id") int id, Authentication authentication) {
        String npmMahasiswa = (String) authentication.getPrincipal();
        PendaftaranAsisten pendaftaranAsisten = pendaftaranAsistenService.getPendaftaranAsisten(id);
        if (pendaftaranAsisten == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        if (!pendaftaranAsisten.getMahasiswa().getNpm().equals(npmMahasiswa)){
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        return ResponseEntity.ok(pendaftaranAsisten);
    }

    @PutMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updatePendaftaranAsisten(@PathVariable(value = "id") int id, @RequestBody PendaftaranAsisten pendaftaranAsisten, Authentication authentication) {
        String npmMahasiswa = (String) authentication.getPrincipal();
        PendaftaranAsisten currentPendaftaranAsisten = pendaftaranAsistenService.getPendaftaranAsisten(id);
        if (!currentPendaftaranAsisten.getMahasiswa().getNpm().equals(npmMahasiswa)){
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        return ResponseEntity.ok(pendaftaranAsistenService.updatePendaftaranAsisten(id, pendaftaranAsisten));
    }

    @DeleteMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity deletePendaftaranAsisten(@PathVariable(value = "id") int id, Authentication authentication) {
        String npmMahasiswa = (String) authentication.getPrincipal();
        PendaftaranAsisten currentPendaftaranAsisten = pendaftaranAsistenService.getPendaftaranAsisten(id);
        if (currentPendaftaranAsisten != null && !currentPendaftaranAsisten.getMahasiswa().getNpm().equals(npmMahasiswa)){
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        pendaftaranAsistenService.deletePendaftaranAsisten(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}

package csui.advpro2021.tais.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import csui.advpro2021.tais.model.LaporanPembayaran;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.PendaftaranAsisten;
import csui.advpro2021.tais.service.LaporanPembayaranService;
import csui.advpro2021.tais.service.MahasiswaService;
import csui.advpro2021.tais.service.PendaftaranAsistenService;

@RestController
@RequestMapping("/laporan-pembayaran")
public class LaporanPembayaranController {
    @Autowired
    private LaporanPembayaranService laporanPembayaranService;

    @Autowired
    private PendaftaranAsistenService pendaftaranAsistenService;

    @Autowired
    private MahasiswaService mahasiswaService;

    @GetMapping(path = "/{npm}/{year}", produces = {"application/json"})
    @ResponseBody
    public Iterable<LaporanPembayaran> getLaporanPembayaran(@PathVariable(value = "npm") String npm, @PathVariable(value = "year") int year, Authentication authentication) {
        String npmMahasiswa = (String) authentication.getPrincipal();
        if (!npmMahasiswa.equals(npm)){
            return null;
        }
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        PendaftaranAsisten pendaftaranAsisten = pendaftaranAsistenService.getPendaftaranAsisten(mahasiswa);
        return laporanPembayaranService.getLaporanPembayaran(pendaftaranAsisten, year);
    }
}

package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.LogAsisten;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.PendaftaranAsisten;
import csui.advpro2021.tais.service.LogAsistenService;
import csui.advpro2021.tais.service.MahasiswaService;
import csui.advpro2021.tais.service.PendaftaranAsistenService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/log-asisten")
public class LogAsistenController {
    @Autowired
    private LogAsistenService logAsistenService;

    @Autowired
    private PendaftaranAsistenService pendaftaranAsistenService;

    @Autowired
    private MahasiswaService mahasiswaService;

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<LogAsisten>> getListLogAsisten(Authentication authentication) {
        String npmMahasiswa = (String) authentication.getPrincipal();
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npmMahasiswa);
        PendaftaranAsisten pendaftaranAsisten = pendaftaranAsistenService.getPendaftaranAsisten(mahasiswa);
        return ResponseEntity.ok(logAsistenService.getListLogAsisten(pendaftaranAsisten));
    }

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createLogAsisten(@RequestBody LogAsisten logAsisten, Authentication authentication) {
        String npmMahasiswa = (String) authentication.getPrincipal();
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npmMahasiswa);
        PendaftaranAsisten pendaftaranAsisten = pendaftaranAsistenService.getPendaftaranAsisten(mahasiswa);
        if (pendaftaranAsisten == null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        if (logAsisten.getPendaftaranAsisten().getId() != pendaftaranAsisten.getId()){
            return new ResponseEntity((HttpStatus.FORBIDDEN));
        }
        if (logAsisten.getEnd().compareTo(logAsisten.getStart()) < 0) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(logAsistenService.createLogAsisten(logAsisten));
    }

    @GetMapping(path = "/{idLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getPendaftaranAsisten(@PathVariable(value = "idLog") int idLog, Authentication authentication) {
        String npmMahasiswa = (String) authentication.getPrincipal();
        LogAsisten logAsisten = logAsistenService.getLogAsisten(idLog);
        if (logAsisten == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        if (!logAsisten.getPendaftaranAsisten().getMahasiswa().getNpm().equals(npmMahasiswa)){
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        return ResponseEntity.ok(logAsisten);
    }

    @PutMapping(path = "/{idLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLogAsisten(@PathVariable(value = "idLog") int idLog, @RequestBody LogAsisten logAsisten, Authentication authentication) {
        String npmMahasiswa = (String) authentication.getPrincipal();
        LogAsisten currentLogAsisten = logAsistenService.getLogAsisten(idLog);
        if (!currentLogAsisten.getPendaftaranAsisten().getMahasiswa().getNpm().equals(npmMahasiswa)){
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        if (pendaftaranAsistenService.getPendaftaranAsisten(logAsisten.getPendaftaranAsisten().getId()) == null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok(logAsistenService.updateLogAsisten(idLog, logAsisten));
    }

    @DeleteMapping(path = "/{idLog}", produces = {"application/json"})
    public ResponseEntity deleteLogAsisten(@PathVariable(value = "idLog") int idLog, Authentication authentication) {
        String npmMahasiswa = (String) authentication.getPrincipal();
        LogAsisten currentLogAsisten = logAsistenService.getLogAsisten(idLog);
        if (currentLogAsisten == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        if (!currentLogAsisten.getPendaftaranAsisten().getMahasiswa().getNpm().equals(npmMahasiswa)){
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        logAsistenService.deleteLogAsisten(idLog);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}

package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.MahasiswaService;
import csui.advpro2021.tais.service.PendaftaranAsistenService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping(path = "/mahasiswa")
public class MahasiswaController {
    @Autowired
    private MahasiswaService mahasiswaService;

    @Autowired
    private PendaftaranAsistenService pendaftaranAsistenService;

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postMahasiswa(@RequestBody Mahasiswa mahasiswa) {
        return ResponseEntity.ok(mahasiswaService.createMahasiswa(mahasiswa));
    }

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getListMahasiswa(Authentication authentication) {
        return ResponseEntity.ok(mahasiswaService.getListMahasiswa());
    }

    @GetMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getMahasiswa(@PathVariable(value = "npm") String npm, Authentication authentication) {
        String npmMahasiswa = (String) authentication.getPrincipal();
        if (!npm.equals(npmMahasiswa)) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(mahasiswa);
    }

    @PutMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateMahasiswa(@PathVariable(value = "npm") String npm, @RequestBody Mahasiswa mahasiswa, Authentication authentication) {
        String npmMahasiswa = (String) authentication.getPrincipal();
        if (!npm.equals(npmMahasiswa)){
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        return ResponseEntity.ok(mahasiswaService.updateMahasiswa(npm, mahasiswa));
    }

    @DeleteMapping(path = "/{npm}", produces = {"application/json"})
    public ResponseEntity deleteMahasiswa(@PathVariable(value = "npm") String npm, Authentication authentication) {
        String npmMahasiswa = (String) authentication.getPrincipal();
        if (!npm.equals(npmMahasiswa)) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (pendaftaranAsistenService.getPendaftaranAsisten(mahasiswa) != null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        mahasiswaService.deleteMahasiswaByNPM(npm);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}

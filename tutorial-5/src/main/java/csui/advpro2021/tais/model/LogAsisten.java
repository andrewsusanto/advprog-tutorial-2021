package csui.advpro2021.tais.model;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "log_asisten")
@Data
public class LogAsisten {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int idLog;

    @ManyToOne
    @JoinColumn(name = "pendaftaran_asisten")
    private PendaftaranAsisten pendaftaranAsisten;

    @Column(name = "start_time", nullable = false)
    private OffsetDateTime start;

    @Column(name = "end_time", nullable = false)
    private OffsetDateTime end;

    @Column(name = "deskripsi")
    private String deskripsi;

    public LogAsisten() {}

    public LogAsisten(int idLog, PendaftaranAsisten pendaftaranAsisten, OffsetDateTime start, OffsetDateTime end, String deskripsi) {
        this.idLog = idLog;
        this.pendaftaranAsisten = pendaftaranAsisten;
        this.start = start;
        this.end = end;
        this.deskripsi = deskripsi;
    }

    public int getIdLog() {
        return this.idLog;
    }

    public PendaftaranAsisten getPendaftaranAsisten(){
        return this.pendaftaranAsisten;
    }

    public OffsetDateTime getStart(){
        return this.start;
    }

    public OffsetDateTime getEnd(){
        return this.end;
    }

    public String getDeskripsi(){
        return this.deskripsi;
    }

    public void setIdLog(int idLog){
        this.idLog = idLog;
    }

    public void setPendaftaranAsisten(PendaftaranAsisten pendaftaranAsisten){
        this.pendaftaranAsisten = pendaftaranAsisten;
    }

    public void setStart(String start) {
        final String pattern = "dd-MM-yyyy HH:mm:ssx";
        DateTimeFormatter dtfB = DateTimeFormatter.ofPattern(pattern);
        try{
            this.start = OffsetDateTime.parse(start, dtfB);
        } catch (Exception e){
            this.start = OffsetDateTime.parse(start + "+07", dtfB);
        }
    }
    
    public void setStart(OffsetDateTime start){
        this.start = start;
    }
    
    public void setEnd(String end) {
        final String pattern = "dd-MM-yyyy HH:mm:ssx";
        DateTimeFormatter dtfB = DateTimeFormatter.ofPattern(pattern);
        try{
            this.end = OffsetDateTime.parse(end, dtfB);
        } catch (Exception e){
            this.end = OffsetDateTime.parse(end + "+07", dtfB);
        }
    }

    public void setEnd(OffsetDateTime end){
        this.end = end;
    }

    public void setDeskripsi(String deskripsi){
        this.deskripsi = deskripsi;
    }
}

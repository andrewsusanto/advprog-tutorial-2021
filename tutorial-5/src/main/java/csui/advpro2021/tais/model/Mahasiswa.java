package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.userdetails.User;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "mahasiswa")
@Data
public class Mahasiswa {

    @Id
    @Column(name = "npm", updatable = false, nullable = false)
    private String npm;

    @Column(name = "nama", nullable = false)
    private String nama;

    @Column(name = "email")
    private String email;

    @Column(name = "ipk")
    private String ipk;

    @Column(name = "no_telp")
    private String noTelp;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name = "password")
    private String password;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToMany(mappedBy = "mahasiswa")
    private List<PendaftaranAsisten> pendaftaranAsisten;

    public Mahasiswa() { }
    
    public Mahasiswa(String npm, String nama, String email, String ipk, String noTelp, String password) {
        this.npm = npm;
        this.nama = nama;
        this.email = email;
        this.ipk = ipk;
        this.noTelp = noTelp;
        this.password = password;
    }

    public String getNpm(){
        return this.npm;
    }

    public String getNama(){
        return this.nama;
    }

    public String getEmail() {
        return this.email;
    }

    public String getIpk(){
        return this.ipk;
    }

    public String getNoTelp(){
        return this.noTelp;
    }

    public String getPassword() { return this.password; }

    public void setNpm(String npm){ this.npm = npm; }

    public void setNama(String nama){
        this.nama = nama;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public void setIpk(String ipk){
        this.ipk = ipk;
    }

    public void setNoTelp(String noTelp){
        this.noTelp = noTelp;
    }

    public void setPassword(String password){
        this.password = password;
    }
}

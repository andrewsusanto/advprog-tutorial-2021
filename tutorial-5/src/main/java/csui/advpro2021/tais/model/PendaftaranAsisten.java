package csui.advpro2021.tais.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "pendaftaran_asisten", uniqueConstraints={@UniqueConstraint(columnNames={"mahasiswa_id"})})
@Data
public class PendaftaranAsisten {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "mahasiswa_id")
    private Mahasiswa mahasiswa;

    @ManyToOne
    @JoinColumn(name = "matakuliah_id")
    private MataKuliah mataKuliah;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToMany(mappedBy = "pendaftaranAsisten")
    private List<LogAsisten> logAsisten;

    public PendaftaranAsisten() {}

    public PendaftaranAsisten(int id, MataKuliah mataKuliah, Mahasiswa mahasiswa) {
        this.id = id;
        this.mataKuliah = mataKuliah;
        this.mahasiswa = mahasiswa;
    }

    public int getId(){
        return this.id;
    }

    public Mahasiswa getMahasiswa() {
        return this.mahasiswa;
    }

    public MataKuliah getMataKuliah() {
        return this.mataKuliah;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setMahasiswa(Mahasiswa mahasiswa){
        this.mahasiswa = mahasiswa;
    }

    public void setMataKuliah(MataKuliah mataKuliah){
        this.mataKuliah = mataKuliah;
    }
}

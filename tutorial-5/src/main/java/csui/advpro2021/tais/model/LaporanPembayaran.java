package csui.advpro2021.tais.model;

import javax.persistence.Entity;

import lombok.Data;

public class LaporanPembayaran {
    private String Month;
    private float jamKerja;
    private int Pembayaran;

    public LaporanPembayaran() {}

    public LaporanPembayaran(String month, float jamKerja, int pembayaran){
        this.Month = month;
        this.jamKerja = jamKerja;
        this.Pembayaran = pembayaran;
    }
    
    public String getMonth() {
        return this.Month;
    }
    
    public void setMonth(String month){
        this.Month = month;
    }

    public float getJamKerja() {
        return this.jamKerja;
    }

    public void setJamKerja(float jamKerja){
        this.jamKerja = jamKerja;
    }

    public int getPembayaran(){
        return this.Pembayaran;
    }

    public void setPembayaran(int pembayaran){
        this.Pembayaran = pembayaran;
    }
}

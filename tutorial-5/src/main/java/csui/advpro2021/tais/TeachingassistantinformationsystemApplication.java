package csui.advpro2021.tais;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.security.authProvider.MahasiswaAuthenticationProvider;
import csui.advpro2021.tais.security.filter.JWTAuthenticationFilter;
import csui.advpro2021.tais.security.filter.JWTAuthorizationFilter;
import csui.advpro2021.tais.service.MahasiswaService;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@SpringBootApplication
public class TeachingassistantinformationsystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeachingassistantinformationsystemApplication.class, args);
    }

    @EnableWebSecurity
	@Configuration
	class WebSecurityConfig extends WebSecurityConfigurerAdapter {

		@Autowired
		private MahasiswaAuthenticationProvider mahasiswaAuthenticationProvider;

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable()
				.authorizeRequests()
				.antMatchers(HttpMethod.POST, "/mahasiswa").permitAll()
				.anyRequest().authenticated()
				.and()
				.addFilterBefore(new JWTAuthenticationFilter(authenticationManager()), UsernamePasswordAuthenticationFilter.class)
				.addFilter(new JWTAuthorizationFilter(authenticationManager()))
				// this disables session creation on Spring Security
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		}

		@Override
		public void configure(AuthenticationManagerBuilder auth) throws Exception {
			auth.authenticationProvider(mahasiswaAuthenticationProvider);
		}
	}
}

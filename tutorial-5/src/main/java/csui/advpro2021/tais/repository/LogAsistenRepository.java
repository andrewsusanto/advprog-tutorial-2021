package csui.advpro2021.tais.repository;

import java.time.OffsetDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import csui.advpro2021.tais.model.LogAsisten;
import csui.advpro2021.tais.model.PendaftaranAsisten;

@Repository
public interface LogAsistenRepository extends JpaRepository<LogAsisten, Integer> {
    public List<LogAsisten> findAllByPendaftaranAsistenAndStartGreaterThanEqualAndEndLessThanEqual(PendaftaranAsisten pendaftaranAsisten, OffsetDateTime start, OffsetDateTime end);
    public List<LogAsisten> findByPendaftaranAsisten(PendaftaranAsisten pendaftaranAsisten);
    public LogAsisten findById(int idLog);
}
package csui.advpro2021.tais.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.PendaftaranAsisten;

@Repository
public interface PendaftaranAsistenRepository extends JpaRepository<PendaftaranAsisten, Integer> {
    public PendaftaranAsisten getByMahasiswa(Mahasiswa mahasiswa);
    public List<PendaftaranAsisten> findByMataKuliah(MataKuliah mataKuliah);
    public PendaftaranAsisten findById(int id);
}
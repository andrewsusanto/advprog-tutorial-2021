package csui.advpro2021.tais.security.filter;

import java.io.IOException;
import java.security.Security;
import java.util.ArrayList;
import java.util.Date;

import javax.security.sasl.AuthenticationException;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.SneakyThrows;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.security.constants.SecurityConstants;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

public class JWTAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private final AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        super(new AntPathRequestMatcher("/mahasiswa/login", "POST"), authenticationManager);
        this.authenticationManager = authenticationManager;
    }

    @SneakyThrows
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) {
        UsernamePasswordAuthenticationToken authRequest = getAuthRequest(req);
        return authenticationManager.authenticate(authRequest);
    }

    public UsernamePasswordAuthenticationToken getAuthRequest(HttpServletRequest req) throws Exception{
        Mahasiswa credentials = new ObjectMapper().readValue(req.getInputStream(), Mahasiswa.class);
        return new UsernamePasswordAuthenticationToken(credentials.getNpm(), credentials.getPassword());
    }

    @Override
    public void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException {
        String token = Jwts.builder()
                .setSubject((String) auth.getPrincipal())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .signWith(SecurityConstants.HKEY)
                .compact();

        String body = token;
        res.getWriter().write(body);
        res.getWriter().flush();
    }
}
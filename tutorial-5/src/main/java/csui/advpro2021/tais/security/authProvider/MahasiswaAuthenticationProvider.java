package csui.advpro2021.tais.security.authProvider;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.MahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class MahasiswaAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    MahasiswaService mahasiswaService;

    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String npm = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null){
            throw new BadCredentialsException("NPM not match any in database");
        }

        boolean passwordMatch = bCryptPasswordEncoder().matches(password, mahasiswa.getPassword());
        if (!passwordMatch){
            throw new BadCredentialsException("Password not match");
        }

        return new UsernamePasswordAuthenticationToken(npm, password, new ArrayList<>());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
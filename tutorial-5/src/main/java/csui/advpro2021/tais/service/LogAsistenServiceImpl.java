package csui.advpro2021.tais.service;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import csui.advpro2021.tais.model.Mahasiswa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import csui.advpro2021.tais.model.LogAsisten;
import csui.advpro2021.tais.model.PendaftaranAsisten;
import csui.advpro2021.tais.repository.LogAsistenRepository;

@Service
public class LogAsistenServiceImpl implements LogAsistenService {
    @Autowired
    private LogAsistenRepository logAsistenRepository;

    @Override
    public Iterable<LogAsisten> getListLogAsisten() {
        return logAsistenRepository.findAll();
    }

    public Iterable<LogAsisten> getListLogAsisten(PendaftaranAsisten pendaftaranAsisten) {
        return logAsistenRepository.findByPendaftaranAsisten(pendaftaranAsisten);
    }

    @Override
    public LogAsisten getLogAsisten(int idLog) {
        return logAsistenRepository.findById(idLog);
    }

    @Override
    public LogAsisten createLogAsisten(LogAsisten logAsisten) {
        logAsistenRepository.save(logAsisten);
        return logAsisten;
    }

    @Override
    public LogAsisten updateLogAsisten(int idLog, LogAsisten logAsisten) {
        logAsisten.setIdLog(idLog);
        logAsistenRepository.save(logAsisten);
        return logAsisten;
    }

    @Override
    public void deleteLogAsisten(int idLog) {
        LogAsisten logAsisten = this.getLogAsisten(idLog);
        logAsistenRepository.delete(logAsisten);
    }

    @Override
    public float getTotalHour(PendaftaranAsisten pendaftaranAsisten, OffsetDateTime start, OffsetDateTime end){
        List<LogAsisten> logAsisten = logAsistenRepository.findAllByPendaftaranAsistenAndStartGreaterThanEqualAndEndLessThanEqual(pendaftaranAsisten, start, end);
        float totalHour = 0;
        for (LogAsisten log : logAsisten){
            OffsetDateTime tempDateTime = OffsetDateTime.from(log.getStart());
            long minutes = tempDateTime.until(log.getEnd(), ChronoUnit.MINUTES);
            totalHour += minutes / 60.0;
        }
        return totalHour;
    }
}

package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.LaporanPembayaran;
import csui.advpro2021.tais.model.LogAsisten;
import csui.advpro2021.tais.model.PendaftaranAsisten;

public interface LaporanPembayaranService {
    Iterable<LaporanPembayaran> getLaporanPembayaran(PendaftaranAsisten pendaftaranAsisten, int year);
}

package csui.advpro2021.tais.service;

import java.time.OffsetDateTime;

import csui.advpro2021.tais.model.LogAsisten;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.PendaftaranAsisten;

public interface LogAsistenService {
    Iterable<LogAsisten> getListLogAsisten();

    Iterable<LogAsisten> getListLogAsisten(PendaftaranAsisten pendaftaranAsisten);

    LogAsisten createLogAsisten(LogAsisten logAsisten);

    LogAsisten getLogAsisten(int idLog);

    LogAsisten updateLogAsisten(int idLog, LogAsisten logAsisten);

    void deleteLogAsisten(int idLog);

    float getTotalHour(PendaftaranAsisten pendaftaranAsisten, OffsetDateTime start, OffsetDateTime end);
}

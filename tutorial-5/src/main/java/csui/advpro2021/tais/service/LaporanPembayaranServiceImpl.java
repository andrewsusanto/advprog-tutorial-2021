package csui.advpro2021.tais.service;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import csui.advpro2021.tais.model.LaporanPembayaran;
import csui.advpro2021.tais.model.LogAsisten;
import csui.advpro2021.tais.model.PendaftaranAsisten;
import csui.advpro2021.tais.repository.LogAsistenRepository;
import static java.time.temporal.TemporalAdjusters.*;

import java.text.DateFormatSymbols;

@Service
public class LaporanPembayaranServiceImpl implements LaporanPembayaranService{
    @Autowired
    private LogAsistenService logAsistenService;

    @Override
    @Transactional
    public List<LaporanPembayaran> getLaporanPembayaran(PendaftaranAsisten pendaftaranAsisten, int year){
        List<LaporanPembayaran> laporanPembayaran = new ArrayList<>();
        for (int i = 1 ; i <= 12 ; i++){
            OffsetDateTime initial = OffsetDateTime.of(year, i, 1, 0, 0, 0, 0, ZoneOffset.of("+07:00"));
            OffsetDateTime start = initial.with(firstDayOfMonth());
            OffsetDateTime end = initial.with(lastDayOfMonth());
            end = end.withHour(23);
            end = end.withMinute(59);
            end = end.withSecond(59);
            end = end.withNano(999);

            float totalHours = logAsistenService.getTotalHour(pendaftaranAsisten, start, end);
            LaporanPembayaran currentMonthReport = new LaporanPembayaran();
            currentMonthReport.setMonth(new DateFormatSymbols().getMonths()[i-1]);
            currentMonthReport.setJamKerja(totalHours);
            currentMonthReport.setPembayaran((int) totalHours * 350);
            if (totalHours != 0.0) {
                laporanPembayaran.add(currentMonthReport);
            }
        }
        return laporanPembayaran;
    }
}

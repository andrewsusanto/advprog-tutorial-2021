package csui.advpro2021.tais.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.PendaftaranAsisten;
import csui.advpro2021.tais.repository.PendaftaranAsistenRepository;

@Service
public class PendaftaranAsistenServiceImpl implements PendaftaranAsistenService {
    @Autowired
    private PendaftaranAsistenRepository pendaftaranAsistenRepository;

    @Override
    public Iterable<PendaftaranAsisten> getListPendaftaranAsisten() {
        return pendaftaranAsistenRepository.findAll();
    }

    @Override
    public PendaftaranAsisten getPendaftaranAsisten(int id) {
        return pendaftaranAsistenRepository.findById(id);
    }

    @Override
    public PendaftaranAsisten getPendaftaranAsisten(Mahasiswa mahasiswa){
        return pendaftaranAsistenRepository.getByMahasiswa(mahasiswa);
    }

    @Override
    public List<PendaftaranAsisten> getPendaftaranAsisten(MataKuliah mataKuliah){
        return pendaftaranAsistenRepository.findByMataKuliah(mataKuliah);
    }

    @Override
    public PendaftaranAsisten createPendaftaranAsisten(PendaftaranAsisten pendaftaranAsisten) {
        pendaftaranAsistenRepository.save(pendaftaranAsisten);
        return pendaftaranAsisten;
    }

    @Override
    public PendaftaranAsisten updatePendaftaranAsisten(int id, PendaftaranAsisten pendaftaranAsisten) {
        pendaftaranAsisten.setId(id);
        pendaftaranAsistenRepository.save(pendaftaranAsisten);
        return pendaftaranAsisten;
    }

    @Override
    public void deletePendaftaranAsisten(int id){
        PendaftaranAsisten pendaftaranAsisten = pendaftaranAsistenRepository.findById(id);
        if (pendaftaranAsisten != null) {
            pendaftaranAsistenRepository.delete(pendaftaranAsisten);
        }
    }
}

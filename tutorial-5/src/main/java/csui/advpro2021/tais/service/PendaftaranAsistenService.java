package csui.advpro2021.tais.service;

import java.util.List;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.PendaftaranAsisten;

public interface PendaftaranAsistenService {
    Iterable<PendaftaranAsisten> getListPendaftaranAsisten();

    PendaftaranAsisten createPendaftaranAsisten(PendaftaranAsisten pendaftaranAsisten);

    PendaftaranAsisten getPendaftaranAsisten(int id);

    PendaftaranAsisten getPendaftaranAsisten(Mahasiswa mahasiswa);

    List<PendaftaranAsisten> getPendaftaranAsisten(MataKuliah mataKuliah);

    PendaftaranAsisten updatePendaftaranAsisten(int id, PendaftaranAsisten pendaftaranAsisten);

    void deletePendaftaranAsisten(int id);
}

package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.LogAsisten;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.PendaftaranAsisten;
import csui.advpro2021.tais.security.authProvider.MahasiswaAuthenticationProvider;
import csui.advpro2021.tais.security.constants.SecurityConstants;
import csui.advpro2021.tais.service.*;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LogAsistenController.class)
public class LogAsistenControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogAsistenServiceImpl logAsistenService;

    @MockBean
    private PendaftaranAsistenServiceImpl pendaftaranAsistenService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @MockBean(answer = Answers.CALLS_REAL_METHODS)
    private MahasiswaAuthenticationProvider mahasiswaAuthenticationProvider;

    private Mahasiswa mahasiswa;
    private LogAsisten logAsisten;
    private PendaftaranAsisten pendaftaranAsisten;
    private MataKuliah mataKuliah;

    @BeforeEach
    public void setUp() throws Exception{
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718", "password");

        mataKuliah = new MataKuliah("ADPROG", "Advanced Programming", "Ilmu Komputer");
        pendaftaranAsisten = new PendaftaranAsisten(1, mataKuliah, mahasiswa);
        OffsetDateTime start = OffsetDateTime.of(2021, 4, 3, 00, 00, 00, 0, ZoneOffset.of("+07:00"));
        OffsetDateTime stop = OffsetDateTime.of(2021, 4, 3, 04, 00, 00, 0, ZoneOffset.of("+07:00"));
        logAsisten = new LogAsisten(1, pendaftaranAsisten, start, stop, "Koreksi Tutorial -5");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private String getJWTToken() {
        return Jwts.builder()
                .setSubject(mahasiswa.getNpm())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .signWith(SecurityConstants.HKEY)
                .compact();
    }

    private String getJWTToken(String npm){
        return Jwts.builder()
                .setSubject(npm)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .signWith(SecurityConstants.HKEY)
                .compact();
    }

    @Test
    public void testControllerPostLog() throws Exception{
        when(logAsistenService.createLogAsisten(any())).thenReturn(logAsisten);
        when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        when(pendaftaranAsistenService.getPendaftaranAsisten(mahasiswa)).thenReturn(pendaftaranAsisten);
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(pendaftaranAsisten);

        String postLogJson = "{\"pendaftaranAsisten\": {\"id\" :1 }, " +
                "   \"start\" : \"02-05-2021 01:40:30\"," +
                "    \"end\" : \"02-05-2021 03:45:30\"," +
                "    \"deskripsi\" : \"test\"" +
                "}";

        mvc.perform(post("/log-asisten")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(postLogJson))
                .andExpect(jsonPath("$.deskripsi").value("Koreksi Tutorial -5"));
    }

    @Test
    public void testControllerPostLogForbiddenPendaftaranAsisten() throws Exception{
        when(logAsistenService.createLogAsisten(any())).thenReturn(logAsisten);
        when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        when(pendaftaranAsistenService.getPendaftaranAsisten(mahasiswa)).thenReturn(pendaftaranAsisten);
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(pendaftaranAsisten);

        String postLogJson = "{\"pendaftaranAsisten\": {\"id\" :2 }, " +
                "   \"start\" : \"02-05-2021 01:40:30\"," +
                "    \"end\" : \"02-05-2021 03:45:30\"," +
                "    \"deskripsi\" : \"test\"" +
                "}";

        mvc.perform(post("/log-asisten")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(postLogJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testControllerPostLogPendaftaranAsistenNotExists() throws Exception{
        when(logAsistenService.createLogAsisten(any())).thenReturn(logAsisten);
        when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        when(pendaftaranAsistenService.getPendaftaranAsisten(any(Mahasiswa.class))).thenReturn(null);

        String postLogJson = "{\"pendaftaranAsisten\": {\"id\" :1 }, " +
                "   \"start\" : \"02-05-2021 01:40:30\"," +
                "    \"end\" : \"02-05-2021 03:45:30\"," +
                "    \"deskripsi\" : \"test\"" +
                "}";

        mvc.perform(post("/log-asisten")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(postLogJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testControllerPostLogDateInvalid() throws Exception{
        when(logAsistenService.createLogAsisten(any())).thenReturn(logAsisten);
        when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        when(pendaftaranAsistenService.getPendaftaranAsisten(mahasiswa)).thenReturn(pendaftaranAsisten);
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(pendaftaranAsisten);

        String postLogJson = "{\"pendaftaranAsisten\": {\"id\" :1 }, " +
                "   \"start\" : \"02-05-2021 04:40:30\"," +
                "    \"end\" : \"02-05-2021 01:45:30\"," +
                "    \"deskripsi\" : \"test\"" +
                "}";

        mvc.perform(post("/log-asisten")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(postLogJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testControllerPostLogWithTimezone() throws Exception{
        when(logAsistenService.createLogAsisten(any())).thenReturn(logAsisten);
        when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        when(pendaftaranAsistenService.getPendaftaranAsisten(mahasiswa)).thenReturn(pendaftaranAsisten);
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(pendaftaranAsisten);

        String postLogJson = "{\"pendaftaranAsisten\": {\"id\" :1 }, " +
                "   \"start\" : \"02-05-2021 01:40:30+07\"," +
                "    \"end\" : \"02-05-2021 03:45:30+07\"," +
                "    \"deskripsi\" : \"test\"" +
                "}";

        mvc.perform(post("/log-asisten")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(postLogJson))
                .andExpect(jsonPath("$.deskripsi").value("Koreksi Tutorial -5"));
    }

    @Test
    public void testControllerGetListLog() throws Exception{
        Iterable<LogAsisten> logAsistens = Arrays.asList(logAsisten);
        when(logAsistenService.getListLogAsisten(any())).thenReturn(logAsistens);
        mvc.perform(get("/log-asisten")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].deskripsi").value("Koreksi Tutorial -5"));
    }

    @Test
    public void testControllerGetLogById() throws Exception{
        when(logAsistenService.getLogAsisten(1)).thenReturn(logAsisten);

        mvc.perform(get("/log-asisten/1")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.deskripsi").value("Koreksi Tutorial -5"));
    }

    @Test
    public void testControllerGetForbiddenLog() throws Exception{
        when(logAsistenService.getLogAsisten(1)).thenReturn(logAsisten);

        mvc.perform(get("/log-asisten/1")
                .header("Authorization", "Bearer " + getJWTToken("1906192054"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }


    @Test
    public void testControllerGetNonExistLog() throws Exception{
        mvc.perform(get("/log-asisten/1")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerUpdateLog() throws Exception{
        logAsistenService.createLogAsisten(logAsisten);
        logAsisten.setDeskripsi("Koreksi Tutorial-6");
        when(logAsistenService.getLogAsisten(1)).thenReturn(logAsisten);
        when(logAsistenService.updateLogAsisten(eq(1), any())).thenReturn(logAsisten);
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(pendaftaranAsisten);

        String mahasiswaUpdateJson = "{\"idLog\":\"1\", \"deskripsi\": \"Koreksi Tutorial-6\", \"start\": \"02-05-2021 01:40:30\", \"end\": \"02-05-2021 03:45:30\"," +
                "\"pendaftaranAsisten\": {\"id\": 1 }}";

        mvc.perform(put("/log-asisten/1")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mahasiswaUpdateJson))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.deskripsi").value("Koreksi Tutorial-6"));
    }

    @Test
    public void testControllerUpdateLogForbidden() throws Exception{
        logAsistenService.createLogAsisten(logAsisten);
        logAsisten.setDeskripsi("Koreksi Tutorial-6");
        when(logAsistenService.getLogAsisten(1)).thenReturn(logAsisten);
        when(logAsistenService.updateLogAsisten(eq(1), any())).thenReturn(logAsisten);
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(pendaftaranAsisten);

        String mahasiswaUpdateJson = "{\"idLog\":\"1\", \"deskripsi\": \"Koreksi Tutorial-6\", \"start\": \"02-05-2021 01:40:30\", \"end\": \"02-05-2021 03:45:30\"," +
                "\"pendaftaranAsisten\": {\"id\": 1 }}";

        mvc.perform(put("/log-asisten/1")
                .header("Authorization", "Bearer " + getJWTToken("1906192054"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(mahasiswaUpdateJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testControllerUpdateLogPendaftaranAsistenDoesNotExists() throws Exception{
        logAsistenService.createLogAsisten(logAsisten);
        logAsisten.setDeskripsi("Koreksi Tutorial-6");
        when(logAsistenService.getLogAsisten(1)).thenReturn(logAsisten);
        when(logAsistenService.updateLogAsisten(eq(1), any())).thenReturn(logAsisten);
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(null);

        String mahasiswaUpdateJson = "{\"idLog\":\"1\", \"deskripsi\": \"Koreksi Tutorial-6\", \"start\": \"02-05-2021 01:40:30\", \"end\": \"02-05-2021 03:45:30\"," +
                "\"pendaftaranAsisten\": {\"id\": 1 }}";

        mvc.perform(put("/log-asisten/1")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mahasiswaUpdateJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testControllerDeleteLog() throws Exception{
        when(logAsistenService.getLogAsisten(1)).thenReturn(logAsisten);
        logAsistenService.createLogAsisten(logAsisten);
        mvc.perform(delete("/log-asisten/1")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testControllerDeleteLogNotExists() throws Exception{
        when(logAsistenService.getLogAsisten(1)).thenReturn(null);
        mvc.perform(delete("/log-asisten/1")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerDeleteLogForbidden() throws Exception{
        when(logAsistenService.getLogAsisten(1)).thenReturn(logAsisten);
        mvc.perform(delete("/log-asisten/1")
                .header("Authorization", "Bearer " + getJWTToken("1906192054"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }
}

package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.PendaftaranAsisten;
import csui.advpro2021.tais.security.authProvider.MahasiswaAuthenticationProvider;
import csui.advpro2021.tais.security.constants.SecurityConstants;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import csui.advpro2021.tais.service.MataKuliahServiceImpl;
import csui.advpro2021.tais.service.PendaftaranAsistenServiceImpl;

import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = MataKuliahController.class)
class MataKuliahControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MataKuliahServiceImpl mataKuliahService;

    @MockBean
    private PendaftaranAsistenServiceImpl pendaftaranAsistenService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @MockBean(answer = Answers.CALLS_REAL_METHODS)
    private MahasiswaAuthenticationProvider mahasiswaAuthenticationProvider;

    private MataKuliah matkul;
    private Mahasiswa mahasiswa;
    private PendaftaranAsisten pendaftaranAsisten;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718", "password");

        matkul = new MataKuliah("ADVPROG", "Advanced Programming", "Ilmu Komputer");
        pendaftaranAsisten = new PendaftaranAsisten(1, matkul, mahasiswa);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private String getJWTToken() {
        return Jwts.builder()
                .setSubject(mahasiswa.getNpm())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .signWith(SecurityConstants.HKEY)
                .compact();
    }

    @Test
    void testControllerGetListMataKuliah() throws Exception {
        Iterable<MataKuliah> listMataKuliah = Arrays.asList(matkul);
        when(mataKuliahService.getListMataKuliah()).thenReturn(listMataKuliah);

        mvc.perform(get("/mata-kuliah")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].kodeMatkul").value(matkul.getKodeMatkul()));
    }

    @Test
    void testControllerCreateMataKuliah() throws Exception {
        when(mataKuliahService.createMataKuliah(matkul)).thenReturn(matkul);

        mvc.perform(post("/mata-kuliah")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(matkul)))
                .andExpect(jsonPath("$.kodeMatkul").value(matkul.getKodeMatkul()));
    }

    @Test
    void testControllerGetMataKuliah() throws Exception {
        when(mataKuliahService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(matkul);
        mvc.perform(get("/mata-kuliah/" + matkul.getKodeMatkul())
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nama").value(matkul.getNama()));
    }

    @Test
    public void testControllerGetNonExistMataKuliah() throws Exception{
        mvc.perform(get("/mata-kuliah/BASDEAD")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void testControllerUpdateMataKuliah() throws Exception {
        mataKuliahService.createMataKuliah(matkul);

        String namaMatkul = "ADV125YIHA";
        matkul.setNama(namaMatkul);

        when(mataKuliahService.updateMataKuliah(matkul.getKodeMatkul(), matkul)).thenReturn(matkul);

        mvc.perform(put("/mata-kuliah/" + matkul.getKodeMatkul())
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(matkul)))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nama").value(namaMatkul));
    }

    @Test
    void testControllerDeleteMataKuliah() throws Exception {
        mataKuliahService.createMataKuliah(matkul);
        mvc.perform(delete("/mata-kuliah/" + matkul.getKodeMatkul())
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void testControllerDeleteMataKuliahPendaftaranAsistenExists() throws Exception {
        mataKuliahService.createMataKuliah(matkul);
        when(mataKuliahService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(matkul);
        List<PendaftaranAsisten> pendaftaranAsistenList = new ArrayList<PendaftaranAsisten>();
        pendaftaranAsistenList.add(pendaftaranAsisten);
        when(pendaftaranAsistenService.getPendaftaranAsisten(matkul)).thenReturn(pendaftaranAsistenList);
        mvc.perform(delete("/mata-kuliah/" + matkul.getKodeMatkul())
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

}
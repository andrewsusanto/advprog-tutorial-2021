package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.PendaftaranAsisten;
import csui.advpro2021.tais.security.authProvider.MahasiswaAuthenticationProvider;
import csui.advpro2021.tais.security.constants.SecurityConstants;
import csui.advpro2021.tais.service.MahasiswaService;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import csui.advpro2021.tais.service.PendaftaranAsistenService;
import csui.advpro2021.tais.service.PendaftaranAsistenServiceImpl;

import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.print.attribute.standard.Media;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = MahasiswaController.class)
public class MahasiswaControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @MockBean
    private PendaftaranAsistenServiceImpl pendaftaranAsistenService;

    @MockBean(answer = Answers.CALLS_REAL_METHODS)
    private MahasiswaAuthenticationProvider mahasiswaAuthenticationProvider;

    private Mahasiswa mahasiswa;
    private String mahasiswaJson;
    private PendaftaranAsisten pendaftaranAsisten;
    private MataKuliah mataKuliah;

    @BeforeEach
    public void setUp() throws Exception{
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718", "password");
        mataKuliah = new MataKuliah("ADPROG", "Advanced Programming", "Ilmu Komputer");
        pendaftaranAsisten = new PendaftaranAsisten(1, mataKuliah, mahasiswa);

        mahasiswaJson = "{\"npm\":\"1906192052\", \"nama\": \"Maung Meong\", \"email\": \"maung@cs.ui.ac.id\", \"ipk\": \"4\"," +
                "\"noTelp\": \"081317691718\", \"password\": \"password\"}";
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private String getJWTToken() {
        return Jwts.builder()
            .setSubject(mahasiswa.getNpm())
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
            .signWith(SecurityConstants.HKEY)
            .compact();
    }

    @Test
    public void testControllerPostMahasiswa() throws Exception{
        when(mahasiswaService.createMahasiswa(mahasiswa)).thenReturn(mahasiswa);
        mvc.perform(post("/mahasiswa")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mahasiswaJson))
                .andExpect(jsonPath("$.npm").value("1906192052"));
    }

    @Test
    public void testControllerGetListMahasiswa() throws Exception{
        Iterable<Mahasiswa> listMahasiswa = Arrays.asList(mahasiswa);
        when(mahasiswaService.getListMahasiswa()).thenReturn(listMahasiswa);
        mvc.perform(get("/mahasiswa")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].npm").value("1906192052"));
    }

    @Test
    public void testControllerGetMahasiswaByNpm() throws Exception{
        when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);

        mvc.perform(get("/mahasiswa/1906192052")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nama").value("Maung Meong"));
    }

    @Test
    public void testControllerGetMahasiswaByNpmForbidden() throws Exception{
        when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);

        mvc.perform(get("/mahasiswa/1906192058")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testControllerGetNonExistMahasiswa() throws Exception{
        mvc.perform(get("/mahasiswa/1906192052")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControllerUpdateMahasiswa() throws Exception{
        mahasiswaService.createMahasiswa(mahasiswa);
        mahasiswa.setNama("Quanta Quantul");
        when(mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa)).thenReturn(mahasiswa);

        String mahasiswaUpdateJson = "{\"npm\":\"1906192052\", \"nama\": \"Quanta Quantul\", \"email\": \"maung@cs.ui.ac.id\", \"ipk\": \"4\"," +
                "\"noTelp\": \"081317691718\", \"password\": \"password\"}";

        mvc.perform(put("/mahasiswa/1906192052")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mahasiswaUpdateJson))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nama").value("Quanta Quantul"));
    }

    @Test
    public void testControllerUpdateMahasiswaForbidden() throws Exception{
        mahasiswaService.createMahasiswa(mahasiswa);
        mahasiswa.setNama("Quanta Quantul");
        when(mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa)).thenReturn(mahasiswa);

        String mahasiswaUpdateJson = "{\"npm\":\"1906192052\", \"nama\": \"Quanta Quantul\", \"email\": \"maung@cs.ui.ac.id\", \"ipk\": \"4\"," +
                "\"noTelp\": \"081317691718\", \"password\": \"password\"}";

        mvc.perform(put("/mahasiswa/1906192055")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mahasiswaUpdateJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testControllerDeleteMahasiswa() throws Exception{
        mahasiswaService.createMahasiswa(mahasiswa);
        mvc.perform(delete("/mahasiswa/1906192052")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testControllerDeleteMahasiswaForbidden() throws Exception{
        mahasiswaService.createMahasiswa(mahasiswa);
        mvc.perform(delete("/mahasiswa/1906192054")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testControllerDeleteMahasiswaPendaftaranExists() throws Exception{
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(pendaftaranAsistenService.getPendaftaranAsisten(mahasiswa)).thenReturn(pendaftaranAsisten);
        mvc.perform(delete("/mahasiswa/1906192052")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

}

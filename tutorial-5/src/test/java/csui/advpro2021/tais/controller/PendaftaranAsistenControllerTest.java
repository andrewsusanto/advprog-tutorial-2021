package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.LogAsisten;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.PendaftaranAsisten;
import csui.advpro2021.tais.security.authProvider.MahasiswaAuthenticationProvider;
import csui.advpro2021.tais.security.constants.SecurityConstants;
import csui.advpro2021.tais.service.LogAsistenServiceImpl;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import csui.advpro2021.tais.service.MataKuliahService;
import csui.advpro2021.tais.service.PendaftaranAsistenServiceImpl;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = PendaftaranAsistenController.class)
public class PendaftaranAsistenControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private PendaftaranAsistenServiceImpl pendaftaranAsistenService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @MockBean
    private MataKuliahService mataKuliahService;

    @MockBean(answer = Answers.CALLS_REAL_METHODS)
    private MahasiswaAuthenticationProvider mahasiswaAuthenticationProvider;

    private Mahasiswa mahasiswa;
    private PendaftaranAsisten pendaftaranAsisten;
    private MataKuliah mataKuliah;
    private MataKuliah mataKuliah2;

    @BeforeEach
    public void setUp() throws Exception{
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718", "password");

        mataKuliah = new MataKuliah("ADPROG", "Advanced Programming", "Ilmu Komputer");
        mataKuliah2 = new MataKuliah("ADPROG0", "Advanced Programming 0", "Ilmu Komputer");
        pendaftaranAsisten = new PendaftaranAsisten(1, mataKuliah, mahasiswa);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private String getJWTToken() {
        return Jwts.builder()
                .setSubject(mahasiswa.getNpm())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .signWith(SecurityConstants.HKEY)
                .compact();
    }

    private String getJWTToken(String npm) {
        return Jwts.builder()
                .setSubject(npm)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .signWith(SecurityConstants.HKEY)
                .compact();
    }

    @Test
    public void testControllerPostPendaftaranAsisten() throws Exception{
        when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        when(mataKuliahService.getMataKuliah("ADPROG")).thenReturn(mataKuliah);
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(null);
        when(pendaftaranAsistenService.createPendaftaranAsisten(any())).thenReturn(pendaftaranAsisten);

        String postLogJson = "{" +
                "    \"mahasiswa\" : {" +
                "        \"npm\" : 1906192052" +
                "    }," +
                "    \"mataKuliah\" : {" +
                "        \"kodeMatkul\" : \"ADPROG\"" +
                "    }" +
                "}";

        mvc.perform(post("/pendaftaran-asisten")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(postLogJson))
                .andExpect(jsonPath("$.mahasiswa.npm").value("1906192052"));
    }

    @Test
    public void testControllerPostPendaftaranAsistenForbidden() throws Exception{
        when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        when(mataKuliahService.getMataKuliah("ADPROG")).thenReturn(mataKuliah);
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(null);
        when(pendaftaranAsistenService.createPendaftaranAsisten(any())).thenReturn(pendaftaranAsisten);

        String postLogJson = "{" +
                "    \"mahasiswa\" : {" +
                "        \"npm\" : 1906192052" +
                "    }," +
                "    \"mataKuliah\" : {" +
                "        \"kodeMatkul\" : \"ADPROG\"" +
                "    }" +
                "}";

        mvc.perform(post("/pendaftaran-asisten")
                .header("Authorization", "Bearer " + getJWTToken("1906192054"))
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(postLogJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testControllerPostPendaftaranAsistenKodeMatkulDoesNotExists() throws Exception{
        when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        when(mataKuliahService.getMataKuliah("ADPROG")).thenReturn(mataKuliah);
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(null);
        when(pendaftaranAsistenService.createPendaftaranAsisten(any())).thenReturn(pendaftaranAsisten);

        String postLogJson = "{" +
                "    \"mahasiswa\" : {" +
                "        \"npm\" : 1906192052" +
                "    }," +
                "    \"mataKuliah\" : {" +
                "        \"kodeMatkul\" : \"ADPROGGGG\"" +
                "    }" +
                "}";

        mvc.perform(post("/pendaftaran-asisten")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(postLogJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testControllerPostPendaftaranAsistenAlreadyRegistered() throws Exception{
        when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        when(mataKuliahService.getMataKuliah("ADPROG")).thenReturn(mataKuliah);
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(null);
        when(pendaftaranAsistenService.createPendaftaranAsisten(any())).thenReturn(pendaftaranAsisten);
        when(pendaftaranAsistenService.getPendaftaranAsisten(any(Mahasiswa.class))).thenReturn(pendaftaranAsisten);

        String postLogJson = "{" +
                "    \"mahasiswa\" : {" +
                "        \"npm\" : 1906192052" +
                "    }," +
                "    \"mataKuliah\" : {" +
                "        \"kodeMatkul\" : \"ADPROG\"" +
                "    }" +
                "}";

        mvc.perform(post("/pendaftaran-asisten")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(postLogJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testControllerGetListPendaftaranAsisten() throws Exception{
        Iterable<PendaftaranAsisten> pendaftaranAsistens = Arrays.asList(pendaftaranAsisten);
        when(pendaftaranAsistenService.getListPendaftaranAsisten()).thenReturn(pendaftaranAsistens);
        mvc.perform(get("/pendaftaran-asisten")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].mahasiswa.npm").value("1906192052"));
    }

    @Test
    public void testControllerGetPendaftaranById() throws Exception{
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(pendaftaranAsisten);

        mvc.perform(get("/pendaftaran-asisten/1")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.mahasiswa.npm").value("1906192052"));
    }

    @Test
    public void testControllerGetPendaftaranByIdForbidden() throws Exception{
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(pendaftaranAsisten);

        mvc.perform(get("/pendaftaran-asisten/1")
                .header("Authorization", "Bearer " + getJWTToken("1906192054"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testControlPendaftaranAsistenNotExists() throws Exception{
        mvc.perform(get("/pendaftaran-asisten/1")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testControlUpdatePendaftaranAsisten() throws Exception{
        pendaftaranAsistenService.createPendaftaranAsisten(pendaftaranAsisten);
        pendaftaranAsisten.setMataKuliah(mataKuliah2);
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(pendaftaranAsisten);
        when(pendaftaranAsistenService.updatePendaftaranAsisten(eq(1), any())).thenReturn(pendaftaranAsisten);

        String pendaftaranUpdateJson = "{" +
                "    \"mahasiswa\" : {" +
                "        \"npm\" : 1906192052" +
                "    },\n" +
                "    \"mataKuliah\" : {" +
                "        \"kodeMatkul\" : \"ADPROG2\"" +
                "    }\n" +
                "}";

        mvc.perform(put("/pendaftaran-asisten/1")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON)
                .content(pendaftaranUpdateJson))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.mataKuliah.kodeMatkul").value("ADPROG0"));
    }

    @Test
    public void testControlUpdatePendaftaranAsistenForbidden() throws Exception{
        pendaftaranAsistenService.createPendaftaranAsisten(pendaftaranAsisten);
        pendaftaranAsisten.setMataKuliah(mataKuliah2);
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(pendaftaranAsisten);
        when(pendaftaranAsistenService.updatePendaftaranAsisten(eq(1), any())).thenReturn(pendaftaranAsisten);

        String pendaftaranUpdateJson = "{" +
                "    \"mahasiswa\" : {" +
                "        \"npm\" : 1906192052" +
                "    },\n" +
                "    \"mataKuliah\" : {" +
                "        \"kodeMatkul\" : \"ADPROG2\"" +
                "    }\n" +
                "}";

        mvc.perform(put("/pendaftaran-asisten/1")
                .header("Authorization", "Bearer " + getJWTToken("1906192054"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(pendaftaranUpdateJson))
                .andExpect(status().isForbidden());
    }

    @Test
    public void testControllerDeletePendaftaranAsisten() throws Exception{
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(pendaftaranAsisten);
        mvc.perform(delete("/pendaftaran-asisten/1")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testControllerDeletePendaftaranAsistenNotFound() throws Exception{
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(null);
        mvc.perform(delete("/pendaftaran-asisten/1")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testControllerDeletePendaftaranAsistenForbidden() throws Exception{
        when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(pendaftaranAsisten);
        mvc.perform(delete("/pendaftaran-asisten/1")
                .header("Authorization", "Bearer " + getJWTToken("1906192054"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }
}

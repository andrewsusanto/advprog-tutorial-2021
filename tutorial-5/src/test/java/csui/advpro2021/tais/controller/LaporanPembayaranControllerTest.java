package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.*;
import csui.advpro2021.tais.security.authProvider.MahasiswaAuthenticationProvider;
import csui.advpro2021.tais.security.constants.SecurityConstants;
import csui.advpro2021.tais.service.*;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LaporanPembayaranController.class)
public class LaporanPembayaranControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private LaporanPembayaranServiceImpl laporanPembayaranService;

    @MockBean
    private PendaftaranAsistenService pendaftaranAsistenService;

    @MockBean
    private MahasiswaService mahasiswaService;

    @MockBean(answer = Answers.CALLS_REAL_METHODS)
    private MahasiswaAuthenticationProvider mahasiswaAuthenticationProvider;

    private Mahasiswa mahasiswa;
    private LogAsisten logAsisten;
    private PendaftaranAsisten pendaftaranAsisten;
    private MataKuliah mataKuliah;
    private LaporanPembayaran laporanPembayaran;

    private String getJWTToken() {
        return Jwts.builder()
                .setSubject(mahasiswa.getNpm())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .signWith(SecurityConstants.HKEY)
                .compact();
    }

    @BeforeEach
    public void setUp() throws Exception{
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718", "password");
        laporanPembayaran = new LaporanPembayaran("January", 10, 350);
    }

    @Test
    public void testGetLaporanPembayaran() throws Exception {
        List<LaporanPembayaran> laporanPembayarans = Arrays.asList(laporanPembayaran);
        when(laporanPembayaranService.getLaporanPembayaran(any(), eq(2021))).thenReturn(laporanPembayarans);
        mvc.perform(get("/laporan-pembayaran/1906192052/2021")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].month").value("January"));
    }

    @Test
    public void testGetLaporanPembayaranForbidden() throws Exception {
        List<LaporanPembayaran> laporanPembayarans = Arrays.asList(laporanPembayaran);
        when(laporanPembayaranService.getLaporanPembayaran(any(), eq(2021))).thenReturn(laporanPembayarans);
        mvc.perform(get("/laporan-pembayaran/1906192054/2021")
                .header("Authorization", "Bearer " + getJWTToken())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}

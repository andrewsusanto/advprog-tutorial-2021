package csui.advpro2021.tais.security.login;

import csui.advpro2021.tais.controller.MahasiswaController;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.security.authProvider.MahasiswaAuthenticationProvider;
import csui.advpro2021.tais.security.filter.JWTAuthenticationFilter;
import csui.advpro2021.tais.service.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = MahasiswaController.class)
public class LoginTest {
    @Autowired
    MockMvc mvc;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @MockBean
    private PendaftaranAsistenServiceImpl pendaftaranAsistenService;

    @MockBean
    private MahasiswaAuthenticationProvider mahasiswaAuthenticationProvider;

    @MockBean
    private MahasiswaRepository mahasiswaRepository;

    @MockBean
    private JWTAuthenticationFilter jwtAuthenticationFilter;

    private Mahasiswa mahasiswa;
    private UsernamePasswordAuthenticationToken userTokenPass;

    @BeforeEach
    public void setup() throws Exception {
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718", "password");

        UsernamePasswordAuthenticationToken userTokenPass = new UsernamePasswordAuthenticationToken("1906192052", "password", new ArrayList<>());
    }

    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @WithMockUser(value = "1906192052", password = "password")
    @Test
    public void testLoginUauthorized() throws Exception {
        String loginJson = "{   " +
                "    \"npm\" : \"1906192052\"," +
                "    \"password\" : \"password\"" +
                "}";

        when(mahasiswaAuthenticationProvider.supports(any())).thenReturn(true);
        when(mahasiswaAuthenticationProvider.bCryptPasswordEncoder()).thenReturn(bCryptPasswordEncoder());
        when(mahasiswaService.createMahasiswa(any())).thenReturn(mahasiswa);
        mahasiswa.setPassword(bCryptPasswordEncoder().encode("password2"));
        when(mahasiswaAuthenticationProvider.authenticate(userTokenPass)).thenReturn(userTokenPass);
        MvcResult mvcResult = mvc.perform(post("/mahasiswa/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(loginJson))
                .andExpect(status().isUnauthorized())
                .andReturn();
    }

    @Test
    public void testAttemptAuthentication() throws  Exception {
        when(jwtAuthenticationFilter.attemptAuthentication(any(), any())).thenReturn(userTokenPass);
        lenient().when(jwtAuthenticationFilter.getAuthRequest(any())).thenReturn(userTokenPass);
        Authentication result = jwtAuthenticationFilter.attemptAuthentication(any(), any());
        Assertions.assertEquals(userTokenPass, result);
    }

    @Test
    public void testSuccessfulAuthentication() throws  Exception {
        jwtAuthenticationFilter.successfulAuthentication(any(), any(), any(), any());
    }

    @Test
    public void testEmptyAuthorization() throws Exception{
        Iterable<Mahasiswa> listMahasiswa = Arrays.asList(mahasiswa);
        when(mahasiswaService.getListMahasiswa()).thenReturn(listMahasiswa);
        mvc.perform(get("/mahasiswa")
                .header("Authorization", "Bearer " )
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

}

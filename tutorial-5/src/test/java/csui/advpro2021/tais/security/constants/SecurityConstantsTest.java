package csui.advpro2021.tais.security.constants;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class SecurityConstantsTest {
    @Mock
    private SecurityConstants securityConstant;

    @Test
    public void testConstructor() throws Exception {
        securityConstant = new SecurityConstants();
    }
}

package csui.advpro2021.tais.security.login;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.security.authProvider.MahasiswaAuthenticationProvider;
import csui.advpro2021.tais.service.MahasiswaService;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration
public class MahasiswaAuthenticationProviderTest {
    @MockBean
    MahasiswaAuthenticationProvider mahasiswaAuthenticationProvider = new MahasiswaAuthenticationProvider();

    @MockBean
    MahasiswaServiceImpl mahasiswaService = new MahasiswaServiceImpl();

    @MockBean(answer = Answers.RETURNS_DEEP_STUBS)
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    private UsernamePasswordAuthenticationToken userTokenPass = new UsernamePasswordAuthenticationToken("1906350692", "password", new ArrayList<>());

    @Test
    public void testSupport() throws Exception {
        boolean result = mahasiswaAuthenticationProvider.supports(UsernamePasswordAuthenticationToken.class);
        Assertions.assertEquals(true, result);
    }

    @Test
    public void testBcrypt() throws Exception {
        BCryptPasswordEncoder bCryptPasswordEncoder = mahasiswaAuthenticationProvider.bCryptPasswordEncoder();
    }
}

package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.PendaftaranAsisten;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.PendaftaranAsistenRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class PendaftaranAsistenServiceImplTest {
    @Mock
    private PendaftaranAsistenRepository pendaftaranAsistenRepository;

    @InjectMocks
    private PendaftaranAsistenServiceImpl pendaftaranAsistenService;

    private Mahasiswa mahasiswa;
    private Mahasiswa mahasiswa2;
    private PendaftaranAsisten pendaftaranAsisten;
    private MataKuliah mataKuliah;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");
        mahasiswa.setPassword("maung2019");

        mataKuliah = new MataKuliah();
        mataKuliah.setKodeMatkul("ADPROG");
        mataKuliah.setNama("Advanced Programming");
        mataKuliah.setProdi("Ilmu Komputer");

        pendaftaranAsisten = new PendaftaranAsisten();
        pendaftaranAsisten.setMataKuliah(mataKuliah);
        pendaftaranAsisten.setMahasiswa(mahasiswa);
    }

    @Test
    public void testServiceCreatePendaftaranAsisten(){
        lenient().when(pendaftaranAsistenService.createPendaftaranAsisten(pendaftaranAsisten)).thenReturn(pendaftaranAsisten);
    }

    @Test
    public void testServiceGetListPendaftaranAsisten(){
        Iterable<PendaftaranAsisten> listPendaftaranAsisten = pendaftaranAsistenRepository.findAll();
        lenient().when(pendaftaranAsistenService.getListPendaftaranAsisten()).thenReturn(listPendaftaranAsisten);
        Iterable<PendaftaranAsisten> listPendaftaranAsistenResult = pendaftaranAsistenService.getListPendaftaranAsisten();
        Assertions.assertIterableEquals(listPendaftaranAsisten, listPendaftaranAsistenResult);
    }

    @Test
    public void testServiceGetListPendaftaranAsistenByMataKuliah(){
        List<PendaftaranAsisten> listPendaftaranAsisten = pendaftaranAsistenRepository.findByMataKuliah(mataKuliah);
        lenient().when(pendaftaranAsistenService.getPendaftaranAsisten(mataKuliah)).thenReturn(listPendaftaranAsisten);
        List<PendaftaranAsisten> listPendaftaranAsistenResult = pendaftaranAsistenService.getPendaftaranAsisten(mataKuliah);
        Assertions.assertIterableEquals(listPendaftaranAsisten, listPendaftaranAsistenResult);
    }

    @Test
    public void testServiceGetPendaftaranAsisten(){
        lenient().when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(pendaftaranAsisten);
        PendaftaranAsisten resultPendaftaranAsisten = pendaftaranAsistenService.getPendaftaranAsisten(1);
        assertEquals(pendaftaranAsisten.getMahasiswa(), resultPendaftaranAsisten.getMahasiswa());
    }

    @Test
    public void testServiceGetPendaftaranAsistenByMahasiswa(){
        lenient().when(pendaftaranAsistenService.getPendaftaranAsisten(mahasiswa)).thenReturn(pendaftaranAsisten);
        PendaftaranAsisten resultPendaftaranAsisten = pendaftaranAsistenService.getPendaftaranAsisten(mahasiswa);
        assertEquals(pendaftaranAsisten, resultPendaftaranAsisten);
    }

    @Test
    public void testServiceDeletePendaftaranAsisten(){
        pendaftaranAsistenService.createPendaftaranAsisten(pendaftaranAsisten);
        pendaftaranAsistenService.deletePendaftaranAsisten(1);
        lenient().when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(null);
        assertEquals(null, pendaftaranAsistenService.getPendaftaranAsisten(1));
    }

    @Test
    public void testServiceDeleteNullPendaftaranAsisten(){
        lenient().when(pendaftaranAsistenRepository.findById(1)).thenReturn(pendaftaranAsisten);
        pendaftaranAsistenService.deletePendaftaranAsisten(1);
        lenient().when(pendaftaranAsistenService.getPendaftaranAsisten(1)).thenReturn(null);
        assertEquals(null, pendaftaranAsistenService.getPendaftaranAsisten(1));
    }

    @Test
    public void testServiceUpdatePendaftaranAsisten(){
        pendaftaranAsistenService.createPendaftaranAsisten(pendaftaranAsisten);
        Mahasiswa currentMahasiswa = pendaftaranAsisten.getMahasiswa();
        // Change to mahasiswa 2
        pendaftaranAsisten.setMahasiswa(mahasiswa2);

        lenient().when(pendaftaranAsistenService.updatePendaftaranAsisten(1, pendaftaranAsisten)).thenReturn(pendaftaranAsisten);
        PendaftaranAsisten resultPendaftaranAsisten = pendaftaranAsistenService.updatePendaftaranAsisten(1, pendaftaranAsisten);

        assertNotEquals(pendaftaranAsisten.getMahasiswa(), currentMahasiswa);
        assertEquals(pendaftaranAsisten.getMahasiswa(), resultPendaftaranAsisten.getMahasiswa());
    }
}

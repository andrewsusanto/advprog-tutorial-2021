package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.*;
import csui.advpro2021.tais.repository.LogAsistenRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.text.DateFormatSymbols;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LaporanPembayaranServiceImplTest {
    @Mock
    private LogAsistenServiceImpl logAsistenService;

    @InjectMocks
    private LaporanPembayaranServiceImpl laporanPembayaranService;

    private LaporanPembayaran laporanPembayaran;
    private Mahasiswa mahasiswa;
    private MataKuliah mataKuliah;
    private PendaftaranAsisten pendaftaranAsisten;
    private LogAsisten logAsisten;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718", "password");

        mataKuliah = new MataKuliah("ADPROG", "Advanced Programming", "Ilmu Komputer");
        pendaftaranAsisten = new PendaftaranAsisten(1, mataKuliah, mahasiswa);
        OffsetDateTime start = OffsetDateTime.of(2021, 4, 3, 00, 00, 00, 0, ZoneOffset.of("+07:00"));
        OffsetDateTime stop = OffsetDateTime.of(2021, 4, 3, 04, 00, 00, 0, ZoneOffset.of("+07:00"));
        logAsisten = new LogAsisten(1, pendaftaranAsisten, start, stop, "Koreksi Tutorial -5");
    }

    @Test
    public void testServiceCreateLaporanPembayaran(){
        when(logAsistenService.getTotalHour(any(), any(), any())).thenReturn(10.0F);
        List<LaporanPembayaran> laporanPembayaranCheck = new ArrayList<>();
        for (int i = 1 ; i <= 12 ; i++){
            LaporanPembayaran x = new LaporanPembayaran(new DateFormatSymbols().getMonths()[i-1], 10, (int) 10*350);
            laporanPembayaranCheck.add(x);
        }
        List<LaporanPembayaran> laporanPembayarans = laporanPembayaranService.getLaporanPembayaran(pendaftaranAsisten, 2021);
        Assertions.assertEquals(10.0F, laporanPembayarans.get(0).getJamKerja());
    }

    @Test
    public void testServiceCreateLaporanPembayaranZero(){
        when(logAsistenService.getTotalHour(any(), any(), any())).thenReturn(0.0F);
        List<LaporanPembayaran> laporanPembayaranCheck = new ArrayList<>();
        for (int i = 1 ; i <= 12 ; i++){
            LaporanPembayaran x = new LaporanPembayaran(new DateFormatSymbols().getMonths()[i-1], 0, (int) 10*350);
            laporanPembayaranCheck.add(x);
        }
        List<LaporanPembayaran> laporanPembayarans = laporanPembayaranService.getLaporanPembayaran(pendaftaranAsisten, 2021);
        Assertions.assertEquals(0, laporanPembayarans.size());
    }

}

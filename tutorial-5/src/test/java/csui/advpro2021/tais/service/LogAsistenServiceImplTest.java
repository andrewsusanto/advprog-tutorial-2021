package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.LogAsisten;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.model.PendaftaranAsisten;
import csui.advpro2021.tais.repository.LogAsistenRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.apache.juli.logging.Log;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LogAsistenServiceImplTest {
    @Mock
    private LogAsistenRepository logAsistenRepository;

    @InjectMocks
    private LogAsistenServiceImpl logAsistenService;

    private Mahasiswa mahasiswa;
    private LogAsisten logAsisten;
    private PendaftaranAsisten pendaftaranAsisten;
    private MataKuliah mataKuliah;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");
        mahasiswa.setPassword("maung2019");

        mataKuliah = new MataKuliah();
        mataKuliah.setKodeMatkul("ADPROG");
        mataKuliah.setNama("Advanced Programming");
        mataKuliah.setProdi("Ilmu Komputer");

        pendaftaranAsisten = new PendaftaranAsisten();
        pendaftaranAsisten.setMataKuliah(mataKuliah);
        pendaftaranAsisten.setMahasiswa(mahasiswa);

        logAsisten = new LogAsisten();

        logAsisten.setPendaftaranAsisten(pendaftaranAsisten);
        logAsisten.setDeskripsi("TUTORIAL ADPROG");

    }

    @Test
    public void testServiceCreateLogAsisten(){
        lenient().when(logAsistenService.createLogAsisten(logAsisten)).thenReturn(logAsisten);
    }

    @Test
    public void testServiceGetListLogAsisten(){
        Iterable<LogAsisten> listLogAsisten = logAsistenRepository.findAll();
        lenient().when(logAsistenService.getListLogAsisten()).thenReturn(listLogAsisten);
        Iterable<LogAsisten> listLogAsistenResult = logAsistenService.getListLogAsisten();
        Assertions.assertIterableEquals(listLogAsisten, listLogAsistenResult);
    }

    @Test
    public void testServiceGetListLogAsistenByPendaftaran(){
        Iterable<LogAsisten> listLogAsisten = logAsistenRepository.findByPendaftaranAsisten(pendaftaranAsisten);
        lenient().when(logAsistenService.getListLogAsisten(pendaftaranAsisten)).thenReturn(listLogAsisten);
        Iterable<LogAsisten> listLogAsistenResult = logAsistenService.getListLogAsisten(pendaftaranAsisten);
        Assertions.assertIterableEquals(listLogAsisten, listLogAsistenResult);
    }

    @Test
    public void testServiceDeleteLog(){
        logAsistenService.createLogAsisten(logAsisten);
        logAsistenService.deleteLogAsisten(1);
        lenient().when(logAsistenService.getLogAsisten(1)).thenReturn(null);
        assertEquals(null, logAsistenService.getLogAsisten(1));
    }

    @Test
    public void testServiceUpdateLog(){
        logAsistenService.createLogAsisten(logAsisten);
        String currentDescription = logAsisten.getDeskripsi();
        //Change IPK from 4 to 3
        logAsisten.setDeskripsi("KOREKSI ADPROG");

        lenient().when(logAsistenService.updateLogAsisten(1, logAsisten)).thenReturn(logAsisten);
        LogAsisten resultLogAsisten = logAsistenService.updateLogAsisten(1, logAsisten);

        assertNotEquals(resultLogAsisten.getDeskripsi(), currentDescription);
        assertEquals(resultLogAsisten.getDeskripsi(), "KOREKSI ADPROG");
    }

    @Test
    public void testServiceGetTotalHour() {
        OffsetDateTime start = OffsetDateTime.of(2021, 4, 2, 0, 0, 0 , 0, ZoneOffset.of("+07:00"));
        OffsetDateTime end = OffsetDateTime.of(2021, 4, 2, 3, 0, 0 , 0, ZoneOffset.of("+07:00"));
        logAsisten.setStart(start);
        logAsisten.setEnd(end);
        List<LogAsisten> logAsistens = new ArrayList<>();
        logAsistens.add(logAsisten);

        when(logAsistenRepository.findAllByPendaftaranAsistenAndStartGreaterThanEqualAndEndLessThanEqual(any(), any(), any())).thenReturn(logAsistens);
        OffsetDateTime startTotal = OffsetDateTime.of(2021, 4, 1, 0, 0, 0 , 0, ZoneOffset.of("+07:00"));
        OffsetDateTime endTotal = OffsetDateTime.of(2021, 4, 30, 23, 0, 0 , 0, ZoneOffset.of("+07:00"));
        float totalHours = logAsistenService.getTotalHour(pendaftaranAsisten, startTotal, endTotal);
        assertEquals(3.0F, totalHours);
    }

}
